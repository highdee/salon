<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\addVendorMail;
use Auth;
use App\User;
use App\vendor;
use App\service;
use App\salon;
use App\employee;
use App\booking;
use App\gallery;
use App\comment;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function vendor_show($salonid){
        $r=vendor::where('salon_id',$salonid)->first();
        $s=service::where('salon_id',$salonid)->get(['name','user_id','salon_id','description','price','time','featured','action']);
        $sa=salon::where('id',$salonid)->get();
        $e=employee::where('salon_id',$salonid)->get();

        $total=0;
        $mo_t=0;
        $sm_t=0;
        $yr_t=0;
        $allBooking=0;
        $fmb=0;$smb=0;$yb=0;

        for ($i=0; $i < count($sa[0]->booking); $i++) { 
            $total=$total + $sa[0]->booking[$i]->price;
            $allBooking=$allBooking + count(json_decode($sa[0]->booking[$i]->services));
        }

        $month=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-1 month')))
                        ->where('salon_id',$salonid)
                        ->get();

        for ($i=0; $i < count($month); $i++) { 
            $mo_t=$mo_t + $month[$i]->price;
            $fmb=$fmb + count(json_decode($month[$i]->services));
        }
        
        $six_m=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-6 month')))
                        ->where('salon_id',$salonid)
                        ->get();
        for ($i=0; $i < count($six_m); $i++) { 
            $sm_t=$sm_t + $six_m[$i]->price;
            $smb=$smb + count(json_decode($six_m[$i]->services));
        }

        $year=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-1 year')))
                        ->where('salon_id',$salonid)
                        ->get();
        for ($i=0; $i < count($year); $i++) { 
            $yr_t=$yr_t + $year[$i]->price;
            $yb=$yb + count(json_decode($year[$i]->services));
        }
        
        return view('admin.vendor_show')->with([
            'vendors'=>$r,
            'services'=>$s,
            'salon'=>$sa,
            'employee'=>$e,
            'amount'=>$total,
            'allBooking'=>$allBooking,
            'fmonthBooking'=>$fmb,'smonthBooking'=>$smb,'yearBooking'=>$yb,
            'fmonth'=>$month, 'smonth'=>$six_m,'year'=>$year,'ftotal'=>$mo_t,'stotal'=>$sm_t,'ytotal'=>$yr_t,
        ]);
    }

    public function salon_show($id){
        $a = salon::find($id);
        $total=0;
        $mo_t=0;
        $sm_t=0;
        $yr_t=0;
        $allBooking=0;
        $fmb=0;$smb=0;$yb=0;

        for ($i=0; $i < count($a->booking); $i++) { 
            $total=$total + $a->booking[$i]->price;
            $allBooking=$allBooking + count(json_decode($a->booking[$i]->services));
        }
        $month=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-1 month')))
                        ->where('salon_id',$id)
                        ->get();
        for ($i=0; $i < count($month); $i++) { 
            $mo_t=$mo_t + $month[$i]->price;
            $fmb=$fmb + count(json_decode($month[$i]->services));
        }
        $six_m=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-6 month')))
                        ->where('salon_id',$id)
                        ->get();
        for ($i=0; $i < count($six_m); $i++) { 
            $sm_t=$sm_t + $six_m[$i]->price;
            $smb=$smb + count(json_decode($six_m[$i]->services));
        }
        $year=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-1 year')))
                        ->where('salon_id',$id)
                        ->get();
        for ($i=0; $i < count($year); $i++) { 
            $yr_t=$yr_t + $year[$i]->price;
            $yb=$yb + count(json_decode($year[$i]->services));
        }

        return view('admin.salon_show')->with([
            'salon'=>$a,'amount'=>$total, 
            'allBooking'=>$allBooking,
            'fmonthBooking'=>$fmb,'smonthBooking'=>$smb,'yearBooking'=>$yb,
            'fmonth'=>$month, 'smonth'=>$six_m,'year'=>$year,
            'ftotal'=>$mo_t,'stotal'=>$sm_t,'ytotal'=>$yr_t
        ]);
    }

    public function addVendor(Request $request){
        $this->validate($request, [
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'username'=>'required|string|unique:vendors,username|unique:users,name',
            'company_name'=>'required|string|unique:vendors',
            'salon_name'=>'required|string|unique:salons,name',
            'email'=>'required|email|unique:users,email',
            'sub_date'=>'required',
            'freq'=>'required',
            'phone'=>'required',
            'password'=>'required',
        ]);   
        
        $due_date='';
        if ($request['sub_date'] == 'year') {
            $due_date=date('Y-m-d',strtotime($request['sub_date'].'+1 year'));
        }else{
            $due_date=date('Y-m-d',strtotime($request['sub_date'].'+1 month'));
        }
         
        $u = new User();
        $u->name=$request['username'] ;
        $u->email=$request['email'];
        $u->password=Hash::make($request['password']);
        $u->contact = $request['phone'];
        $u->save();

        $v = new vendor();
        $v->first_name=$request['first_name'];
        $v->last_name=$request['last_name'];
        $v->username=$request['username'];
        $v->company_name=$request['company_name'];
        $v->phone = $request['phone'];
        $v->user_id=$u->id;
        $v->sub_date=$request['sub_date'];
        $v->freq=$request['freq'];
        $v->due_date=$due_date;
        $v->verified = now();

        $salon=new salon();
        $salon->name=$request['salon_name'];
        $salon->user_id = $u->id;
        $salon->save();

        $v->salon_id=$salon->id;
        $v->save();

        $payload=[
            'fname'=>$request['first_name'],
            'lname'=>$request['last_name'],
            'email'=>$request['email'],
            'username'=>$request['username'],
            'company'=>$request['company_name'],
            'phone'=>$request['phone'],
            'salon'=>$request['salon_name'],
            'sub_date'=>$request['sub_date'],
            'sub_type'=>$request['freq'],
            'password'=>$request['password']    
        ];

        Mail::to($request['email'])->sendNow(new addVendorMail($payload));

        session()->flash('message', 'Vendor Saved Successfully');
        session()->flash('type', 'success');
        return redirect()->back();
    }
}
