<?php

namespace App\Http\Controllers;

use App\booking;
use App\comment;
use App\employee;
use App\Mail\bookingNotification;
use App\Mail\notification;
use App\salon;
use App\schedule;
use App\service;
use App\shift;
use App\User;
use App\vendor;
use App\workinghours;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\JWTAuth;

class userController extends Controller
{
    //
    public function __construct(){
        $this->middleware('jwt.auth');
    }

    public function dashboard(){
        $bookings=booking::where('user_id',Auth::user()->id)->with('customer')->orderBy('id','desc')    ->get();

        return view('biz.user-panel')->with([
            'bookings' => $bookings
        ]);
    }

    public function checkFavorite($id,$salon){
        $salon=salon::where('id',$salon)->first();
        $check=$salon->favorites()->where('user_id',$id)->first();

        return $check != null ? 1:0;
    }

    public function addFavorite(Request $request,$user,$id){
        $user=User::find($user);
        $salon=salon::find($id);

        if($salon){
            $user->salonFavorite()->detach($salon);
            $user->salonFavorite()->attach($salon);

            return response(200);
        }
    }

    public function removeFavorite(Request $request,$user,$id){
        $user=User::find($user);
        $salon=salon::find($id);

        if($salon){
            $user->salonFavorite()->detach($salon);

            return response(200);
        }
    }

    public function place_order(Request $request){
        $request->session()->flash('order',1);
        $order=$request->input('order');
        $order=json_decode($order);

        if($order->date == '' || !isset($order->date)){
            $this->setAlert(null,'danger','Please pick a date');

            return redirect()->back();
        }
        if($order->time == '' || !isset($order->time)){
            $this->setAlert(null,'danger','Please pick a time');

            return redirect()->back();
        }
        if(!isset($order->services)){
            $this->setAlert(null,'danger','Please pick a service');

            return redirect()->back();
        }
        if(!isset($order->salon)){
            $this->setAlert(null,'danger','Please pick a salon');

            return redirect()->back();
        }

        $book=new booking();
        $book->user_id=Auth::user()->id;
        $book->salon_id=$order->salon->id;
        $book->employee_id=$order->empl;

        $book->user_name=Auth::user()->username;

        $book->date=$order->date;
        $book->time=$order->time;
        $book->date=$order->date;
        $book->note=$order->note;
        $book->status=0;
        $price=0;
        $details=[];
        foreach ($order->services as $service){
            $price+=(int) $service->price;
            $details[]=['id'=>$service->id,'name'=>$service->name,'price'=>$service->price];
        }
        $book->price=$price;
        $book->services=json_encode($details);
        $book->save();


        $request->session()->flash('order-success',1);
        $this->setAlert(null,'success','Your order has been placed successfully.');
        return redirect()->back();
    }

    public function getUser(Request $request){
        $user=User::where('id',auth::user()->id)->with('salon','salon.employees','salon.employees.booking')->with('vendor')->first();
        return $user;
    }

    public function book(Request $request){
        $this->validate($request,[
            'employee' => 'required',
            'services' => 'required',
            'day' => 'required',
            'time' => 'required',
            'salon' => 'required',
        ]);

        $data=$request->input();
        $user=Auth::user();

//        if(!empty($data['email'])){
//            $user=User::where('email',$data['email'])->where('email','!=',$user->email)->first();
//            if(!$user){
//                return response([
//                    'status' => false,
//                    'message' => 'No customer with this email',
//                ],200);
//            }
//        }

        $salon=salon::find($data['salon']);
        if(!$salon){
            return response([
                'status' => false,
                'message' => 'Salon not found',
            ],200);
        }

        $employee=employee::find($data['employee']);
        if(!$employee){
            return response([
                'status' => false,
                'message' => 'Employee not found',
            ],200);
        }

        $check=booking::
        where(['employee_id'=>$employee->id,'date'=>$data['day'],'time'=>$data['time'],'status'=>0])->
        where(['employee_id'=>$employee->id,'date'=>$data['day'],'time'=>$data['time'],'status'=>1])
            ->first();
        if($check){
            return response([
                'status' => false,
                'message' => 'Sorry employee has been booked at this time.',
            ],200);
        }


        $services=[];
        $price=0;
        $time=0;
        foreach ($data['services'] as $serv){
            $service=service::find($serv['id']);
            if($service){
                $price += $service->price;
                $time += $service->time;
                $services[]=['id'=>$service->id,'name'=>$service->name,'price'=>$service->price,'time'=>$service->time];
            }
        }
        $booking=new booking();
        $booking->user_id=$user->id;
        $booking->salon_id=$salon->id;
        $booking->employee_id=$employee->id;
        $booking->date=$data['day'];
        $booking->time=$data['time'];
        $booking->price=$price;
        $booking->note=$data['note'];
        $booking->services=json_encode($services);
        $booking->status=0;
        $booking->save();

        if(empty($data['email']) && $salon->owner()->first()->email != $user->email) {
            $link = env('APP_URL_FRONTEND') . "/vendor#terminiNav";
            $data = [
                'user' => $salon->name,
                'date' => $data['day'] . ' ' . $data['time'],
                'link' => $link
            ];
            Mail::to($salon->owner()->first()->email)->send(new bookingNotification($data));
        }

        if($salon->owner()->first()->email != $user->email){
            $data2=[
                'user'=> $user->name,
                'text'=>'Your booking has been submitted you will be notified once your booking has been approved.'
            ];
            Mail::to($user->email)->send(new notification($data2));
        }

        return response([
            'status' => true,
            'message' => 'Your booking was successfull',
            'data'  => $booking
        ],200);
    }

    public function getUserBooking(){

        $user=auth::user();

        $bookings=booking::where(['user_id'=>$user->id])->with('salon')->where('deleted_at',null)->orderBy('id','desc')->paginate(5);

        return $bookings;
    }

    public function getPendingBooking(){

        $user=auth::user();

        $bookings=booking::where(['salon_id'=>$user->salon()->first()->id,'status'=>0])->with('customer')->with('salon')->orderBy('id','desc')->paginate(5);

        return $bookings;
    }

    public function deleteBooking($id){
        $user=auth::user();

        $book=booking::where(['user_id'=>$user->id,'id'=>$id])->first();

        if(!$book){
            return response('',404);
        }

        if($book->status != -1){
            //            MAIL SENDING TO VENDOR
            $data2=[
                'user'=> $user->name,
                'text'=>" <h4>Your appointment with ".$user->name." has been canceled.</h4>
                <table>
                        <tr>
                                 <td>Service:</td><td>".$book->service_names."</td>
                         </tr>
                              <tr>   <td>Employee:</td><td>".$book->employee->name."</td></tr>
                                 <tr><td>Date:</td><td>".$book->date."</td></tr>
                                 <tr><td>Time:</td><td>".$book->time."</td></tr>
                                 <tr><td>Price:</td><td>".$book->price." kn</td></tr>               
                          
                </table> 
                <h3>NOTE: This is an action by the customer</h3>
            "
            ];
            Mail::to($book->salon->owner()->first()->email)->send(new notification($data2));

//            MAIL SENDING TO CUSTOMER
            $data2=[
                'user'=> $user->name,
                'text'=>" <h4>You have canceled your appointment with ".$book->salon->name." has been canceled.</h4>
                <table>
                        <tr>
                                 <td>Service:</td><td>".$book->service_names."</td>
                         </tr>
                              <tr>   <td>Employee:</td><td>".$book->employee->name."</td></tr>
                                 <tr><td>Date:</td><td>".$book->date."</td></tr>
                                 <tr><td>Time:</td><td>".$book->time."</td></tr>
                                 <tr><td>Price:</td><td>".$book->price." kn</td></tr>               
                          
                </table> 
                <h3>NOTE: This is an action by the you</h3>
            "
            ];
            Mail::to($user->email)->send(new notification($data2));


        }

        if($book->status != 1){
            $book->deleted_at=date('Y-m-d');
        }
        $book->status=-1;
        $book->save();
        return response('',200);
    }

    public function getMonthBooking(Request $request){
        $data=$request->input();

        $bookings=[];
        $bookings[$data['year'].'-'.$data['month']]=booking::where(['salon_id'=>$data['salon']])->whereMonth('date','=',$data['month'])->whereYear('date','=',$data['year'])->where('status','!=','-1')->where('status','!=','2')->get();
        $bookings[$data['year'].'-'.((int)$data['month']-1)]=booking::where(['salon_id'=>$data['salon']])->whereMonth('date','=',$data['month']-1)->whereYear('date','=',$data['year'])->where('status','!=','-1')->where('status','!=','2')->get();

        return $bookings;
    }


    public function getBoookingHead($date){
        $user=auth::user();
        $salon=salon::find($user->salon->id);

        if(!$salon){
            return response('',404);
        }

        $bookings=[];
        $bookings['head']=booking::where('deleted_at',null)->where(['salon_id'=>$salon->id,'status'=>0])->with('customer')->orderBy('id','desc')->limit(5)->get();
        $bookings['body']=booking::where('deleted_at',null)->where(['salon_id'=>$salon->id,'status'=>1])->whereDate('date',date('Y-m-d',strtotime($date)))->orWhere(['salon_id'=>$salon->id,'status'=>2])->whereDate('date',date('Y-m-d',strtotime($date)))->with('customer')->orderBy('created_at','asc')->get();

        return [
            'data'=>$bookings,
            'date'=> $date
        ];
    }

    public function acceptBooking(Request $request , $id){
        $user=auth::user();

        $book=booking::find($id);
        if(!$book){
            return response('',404);
        }

        if($book->salon_id != $user->salon()->first()->id){
            return response('',404);
        }

        if(date('Y-m-d') > $book->date){

            return response(
                [
                    'status'=>false,
                    'message'=>"You can't approve this appoinment again, because the time has pass",
                ]
            );
        }

        if($user->id != $book->customer()->first()->id){

            $data2=[
                'user'=> $user->name,
                'text'=>" <h4>Your appointment with ".$book->salon()->first()->name." has been accepted.</h4>
                <table>
                        <tr>
                                 <td>Service:</td><td>".$book->service_names."</td>
                         </tr>
                              <tr>   <td>Employee:</td><td>".$book->employee->name."</td></tr>
                                 <tr><td>Date:</td><td>".$book->date."</td></tr>
                                 <tr><td>Time:</td><td>".$book->time."</td></tr>
                                 <tr><td>Price:</td><td>".$book->price." kn</td></tr>               
                          
                </table>
                <br>
                <fieldset>
                    <b>MESSAGE FROM SALON:</b>
                    <hr>
                    <p>".$request->input('note')."</p>
                </fieldset>
            "
            ];
            Mail::to($book->customer()->first()->email)->send(new notification($data2));

        }
        $book->vendor_note=$request->input('note');
        $book->status=1;
        $book->save();


        return response([
            'status'=>true,
            'message'=>'Booking was accepted successfully',
        ]);
    }

    public function rejectBooking(Request $request,$id){
        $user=auth::user();

        $book=booking::find($id);
        if(!$book){
            return response('',404);
        }

        if($book->salon_id != $user->salon()->first()->id){
            return response('',404);
        }



        $data2=[
            'user'=> $user->name,
            'text'=>"
                    Your appoinment with ".$user->salon->name." (".$book->service_names.") has been rejected.
                     <br>
                    <fieldset>
                        <b>MESSAGE FROM SALON:</b>
                        <hr>
                        <p>".$request->input('note')."</p>
                    </fieldset>
            "
        ];
        Mail::to($book->customer()->first()->email)->send(new notification($data2));

        $book->vendor_note=$request->input('note');
        $book->status=-1;
        $book->save();

        $bookings=[];
        $bookings['head']=booking::where('deleted_at',null)->where(['salon_id'=>$user->salon()->first()->id,'status'=>0])->with('customer')->orderBy('id','desc')->limit(5)->get();

        return response([
            'status'=>true,
            'message'=>'Booking was reject successfully',
            'data'=>$bookings
        ]);
    }

    public function getBooking(){
        $user=auth::user();

        $bookings=booking::where('deleted_at',null)->where('status',0)->where(['salon_id'=>$user->salon()->first()->id])->with('customer')->with('salon')->orderBy('id','desc')->paginate(5);

        return $bookings;
    }
    public function finishBooking(Request $request,$id){
        $user=auth::user();

        $book=booking::find($id);
        if(!$book){
            return response('',404);
        }

        if($book->salon_id != $user->salon()->first()->id){
            return response('',404);
        }

        $book->status=2;
        $book->save();

        $bookings=[];
        $bookings['head']=booking::where('deleted_at',null)->where(['salon_id'=>$user->salon()->first()->id,'status'=>0])->with('customer')->orderBy('id','desc')->limit(5)->get();

        return response([
            'status'=>true,
            'message'=>'Booking was reject successfully',
            'data'=>$bookings
        ]);
    }


    public function getUserComment($id){
        $comments=comment::where(['user_id'=>$id,'parent_id'=>0])->with('salon')->with('user')->orderBy('id','desc')->paginate(5);

        return $comments;
    }

    public function updateUserComment(Request $request,$id){
        $this->validate($request,['text'=>'required']);
        $user=auth::user();

        $commment=comment::where(['user_id'=>$user->id,'id'=>$id])->first();
        if($commment){
            $commment->text=$request->input('text');
            $commment->save();
        }

        return response('updated',200);
    }

    public function deleteUserComment(Request $request,$id){
        $user=auth::user();

        $commment=comment::where(['user_id'=>$user->id,'id'=>$id])->first();
        if($commment){
            $commment->delete();
        }

        return response('Deleted',200);
    }

    public function getFavoriteSalons(){
        $user=auth::user();
        $data=$user->salonFavorite()->paginate(10);

        return $data;
    }
    public function removeFavoriteSalon($id){
        $user=auth::user();
        $data=$user->salonFavorite()->detach($id);

        return $data;
    }

    public function userHasComment($salon){
        $user=auth::user();

        $check=booking::where(['user_id'=>$user->id,'reviewed'=>null,'salon_id'=>$salon,'status'=>2])->first();
        return $check;
        return response([
            'status'=>$check ? true:false
        ]);
    }
    public function commentSalon(Request $request){
        $this->validate($request,[
            'text'=>'required',
            'user'=>'required',
            'salon'=>'required',
            'rating'=>'required',
        ]);

        $data=$request->input();
        $user=auth::user();
        $comment=new comment();
        $comment->user_id=$user->id;
        $comment->salon_id=$data['salon'];
        $comment->text=$data['text'];
        $comment->rating=$data['rating'];
        $comment->parent_id=0;

        $comment->save();

        $comments=booking::where(['user_id'=>$user->id,'reviewed'=>null,'salon_id'=>$data['salon'],'status'=>2])->get();
        foreach ($comments as $cmt) {
            $cmt->reviewed=date('Y-m-d');
            $cmt->save();
        }

        $comment=comment::where('id',$comment->id)->with('user')->first();

        return response([
            'status'=>true,
            'data'=>$comment
        ]);


    }

    public function workingHours(Request $request){
        $user=auth::user();

        $data=json_decode($request->input('data'));

        foreach ($data as $day){
            $working_hours=workinghours::where(['salon_id'=>$user->salon->id,'day'=>$day->value])->first();

            if(!$working_hours){
                $working_hours=new workinghours();
                $working_hours->salon_id=$user->salon->id;
            }

            $working_hours->day=$day->value;
            $working_hours->start=strlen($day->data->start) == 0 ? 0:$day->data->start;
            $working_hours->ps=strlen($day->data->ps) == 0 ? 0:$day->data->ps;
            $working_hours->pk=strlen($day->data->pk) == 0 ? 0:$day->data->pk;
            $working_hours->end=strlen($day->data->end ) == 0 ? 0:$day->data->end ;

            $working_hours->save();
        }

        $user=User::find($user->id);

        return response([
            'status'=>true,
            'message'=>"Working hours was updated",
            'data'=>$user
        ]);
    }

    public function addshift(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'color'=>'required',
            'start'=>'required',
            'end'=>'required',
        ]);

        $user=auth::user();
        $details=$request->input();

        $shift=new shift();
        $shift->salon_id=$user->salon->id;
        $shift->name=$details['name'];
        $shift->color=$details['color'];
        $shift->start=$details['start'];
        $shift->end=$details['end'];

        $shift->save();

        $user=User::find($user->id);

        return response([
            'status'=>true,
            'message'=>"Shift was created",
            'data'=>$user
        ]);
    }

    public function deleteShift($id){
        $user=auth::user();

        $shift=shift::where(['salon_id'=>$user->salon->id,'id'=>$id])->first();
        if($shift){
            $shift->delete();
        }

        $user=User::find($user->id);

        return response([
            'status'=>true,
            'message'=>"Shift was deleted",
            'data'=>$user
        ]);
    }

    public function getIfBookingIsWithinShift($start,$end,$booked_time){
        $start=explode(':',$start);
        $end=explode(':',$end);
        $booked_time=explode(':',$booked_time);
        $start_in_mins=((int)$start[0] * 60 ) + ((int)$start[1]);
        $end_in_mins=((int)$end[0] * 60 ) + ((int)$end[1]);
        $booked_time_in_mins=((int)$booked_time[0] * 60 ) + ((int)$booked_time[1]);


//        return [$start_in_mins,$booked_time_in_mins,$end_in_mins];

        if($start_in_mins <= $booked_time_in_mins && $booked_time_in_mins <= $end_in_mins){
            return true;
        }
        return false;
    }

    public function schedule(Request $request){


        $user=auth::user();

        $data=json_decode($request->input('data'));
        $affected_bookings=[];

        foreach ($data  as $row){
            foreach ($row->schedules as $key => $sched){
                $schedule=schedule::where(['salon_id'=>$user->salon->id,'day'=>$sched->day,'employee_id'=>$row->employee->id,'week'=>$row->week,'year'=>$row->year])->first();
//                $schedule_check1=$schedule->whereDate('date','<',date('Y-m-d'))->first();

                if(!$schedule){
                   $schedule=new schedule();
                   $schedule->salon_id=$user->salon->id;
                   $schedule->employee_id=$row->employee->id;
                   $schedule->week=$row->week;
                   $schedule->year=$row->year;
                   $schedule->date = date('Y-m-d', strtotime($row->start . ($key + 1) . ' day'));
                }
                else{

                    //checking if the schedule update doesn't affect already booked time and date
                    $bookings=booking::where(['employee_id' => $row->employee->id,'date'=>$schedule->date])->where(function ($q){ $q->where('status',0)->orWhere('status',1);});
                    $bookings=$bookings->whereDate('date','>=',date('Y-m-d'))->get();
                    $new_shift=shift::find($sched->value);
                    $old_shift=shift::find($schedule->shift_id);


                    if(count($bookings) > 0 && $schedule->shift_id != $sched->value){
                        $temp=[];

                        foreach ($bookings as $booking){
                            if(!$this->getIfBookingIsWithinShift($new_shift->start,$new_shift->end,$booking->time)){
//                                return response([$new_shift,$row->employee->id,$bookings,$schedule]);
                                $temp[]=$booking;
                            }
                        }
//                        return response($temp);
                        if(count($temp) > 0){
                            $affected_bookings[]=['id'=>$booking->employee->id,'shift'=>$old_shift,'bookings'=>$temp,'employee'=>$booking->employee,'day'=>$schedule->day];
                        }
                    }
                }

                if(count($affected_bookings) <= 0) {
                    $schedule->shift_id = $sched->value;
                    $schedule->day = $sched->day;
                    $schedule->save();
                }
            }
        }

        if(count($affected_bookings) > 0){
            return response([
                'status'=>false,
                'message'=>"Some employee's booking will be affected by this change in schedule",
                'affecteds'=>$affected_bookings
            ],200);
        }
        $user=User::find($user->id);

        return response([
            'status'=>true,
            'message'=>"Schedule was updated",
            'data'=>$user
        ]);
    }

    public function getSchedule($week,$year){
        $user=auth::user();

        $schedule=schedule::where(['salon_id'=>$user->salon->id,'week'=>$week,'year'=>$year])->get();

        return [
            'schedules'=>$schedule,
            'week'=>$week,
            'year'=>$year,
        ];
    }

}

