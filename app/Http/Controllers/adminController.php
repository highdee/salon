<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\addVendorMail;
use Auth;
use App\User;
use App\vendor;
use App\service;
use App\salon;
use App\employee;
use App\booking;
use App\gallery;
use App\comment;
use DB;


class adminController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    } 

    public function index(){
        $user=DB::table('users AS u')
            ->select('u.id','u.name','u.contact','u.whatsapp','u.email','u.address','u.status')
            ->leftJoin('vendors', 'vendors.user_id', '=', 'u.id')
            ->whereNull('vendors.user_id')->get()->count();
        // $user=User::get()->count();
        $salon=salon::get()->count();
        $vendor=vendor::get()->count();
        $booking=booking::get()->count();
        //dd($salon);
    	return view('admin.index')->with([
            'user'=>$user,
            'vendor'=>$vendor,
            'salon'=>$salon,
            'booking'=>$booking
        ]);
    }

    public function vendorList(){
        $r = Vendor::all();
        return view('admin.vendor')->with('vendors', $r);
    }
    
    public function userList(){
        $u=DB::table('users AS u')
            ->select('u.id','u.name','u.contact','u.whatsapp','u.email','u.address','u.status')
            ->leftJoin('vendors', 'vendors.user_id', '=', 'u.id')
            ->whereNull('vendors.user_id')->get();    
                                                    
        return view('admin.user')->with('users',$u);
    }
    public function user_show($userid){
        $r=User::find($userid);
        $u=booking::where('user_id',$userid)
                    ->where('date', '<', date("Y-m-d"))
                    ->where('status', '!=', 2)
                    ->get();
        return view('admin.user_show')->with(['user'=>$r,'show'=>$u]);  
    }

    
    public function logout() {
	  Auth::logout();
	  return redirect('/adminLogin');
	}

    
    public function salon(){
        $a = salon::all();
        return view('admin.salon')->with('salons', $a);
    }
    public function salon_show($id){
        $a = salon::find($id);
        $total=0;
        $mo_t=0;
        $sm_t=0;
        $yr_t=0;
        $allBooking=0;
        $fmb=0;$smb=0;$yb=0;

        for ($i=0; $i < count($a->booking); $i++) { 
            $total=$total + $a->booking[$i]->price;
            $allBooking=$allBooking + count(json_decode($a->booking[$i]->services));
        }
        $month=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-1 month')))
                        ->where('salon_id',$id)
                        ->get();
        for ($i=0; $i < count($month); $i++) { 
            $mo_t=$mo_t + $month[$i]->price;
            $fmb=$fmb + count(json_decode($month[$i]->services));
        }
        $six_m=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-6 month')))
                        ->where('salon_id',$id)
                        ->get();
        for ($i=0; $i < count($six_m); $i++) { 
            $sm_t=$sm_t + $six_m[$i]->price;
            $smb=$smb + count(json_decode($six_m[$i]->services));
        }
        $year=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-1 year')))
                        ->where('salon_id',$id)
                        ->get();
        for ($i=0; $i < count($year); $i++) { 
            $yr_t=$yr_t + $year[$i]->price;
            $yb=$yb + count(json_decode($year[$i]->services));
        }

        return view('admin.salon_show')->with([
            'salon'=>$a,'amount'=>$total, 
            'allBooking'=>$allBooking,
            'fmonthBooking'=>$fmb,'smonthBooking'=>$smb,'yearBooking'=>$yb,
            'fmonth'=>$month, 'smonth'=>$six_m,'year'=>$year,
            'ftotal'=>$mo_t,'stotal'=>$sm_t,'ytotal'=>$yr_t
        ]);
    }
    public function setFeatured(Request $request){
        $get = salon::find($request['salon_id']);
        $get->featured = !$get->featured;
        if(!$get->save()){
            session()->flash('message', 'Featured not set');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        // $this->salon();
        session()->flash('message', 'Featured set successful');
        session()->flash('type', 'success');
        return redirect()->back();
        
    }

    public function deleteUser($id){
        $user = User::find($id);
        if(!$user->delete()){
            session()->flash('message', 'User not deleted Successfully');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        session()->flash('message', 'User Delete successful');
        session()->flash('type', 'success');
        return redirect()->route('admin.user');
    }

    public function banUser(Request $request){
        $u = $this->validate($request,[
            'user_id'=>'required'
        ]);
        $user=User::find($u['user_id']);
        if(!user){
            session()->flash('message', 'User not found');
            session()->flash('type', 'error');
            return redirect()->back(); 
        }
        $user->status = -($user->status) - 1;
        if(!$user->save()){
            session()->flash('message', 'User not Ban, Pls Try Again');
            session()->flash('type', 'error');
            return redirect()->back();   
        }
        session()->flash('message', 'Successfully');
        session()->flash('type', 'success');
        return redirect()->back();
    }
    
    public function banVendor(Request $request){
        $u = $this->validate($request,[
            'user_id'=>'required'
        ]);
        $user=User::find($u['user_id']);
        $sn=salon::where('user_id',$u['user_id'])->first();
        // $msg;
        if($user->status == -1){
            $user->status = 0;
            $sn->status = 0;
            $msg='Owner Unbanned Successful';
        }else{
            $user->status = -1;
            $sn->status = -1;
            $msg='Owner Banned Successful';
        }

        if(!$user->save() || !$sn->save()){
            session()->flash('message', 'Vendor not Ban, Pls Try Again');
            session()->flash('type', 'error');
            return redirect()->back();   
        }

        // $user->status = -($user->status) - 1;

        // $v=vendor::where('user_id',$u['user_id'])->first();
        // $v->status = 1 - $v->status;

        // $sal=salon::where('user_id',$u['user_id'])->first();
        // $sal->features= 1 - $sal->features;

        // if(!$user->save() || !$v->save() || !$sal->save()){
        //     session()->flash('message', 'Vendor not Ban, Pls Try Again');
        //     session()->flash('type', 'error');
        //     return redirect()->back();   
        // }
        session()->flash('message', $msg);
        session()->flash('type', 'success');
        return redirect()->back();
    }
    public function deleteVendor($id){
        $user = User::find($id);
        $s=salon::where('user_id', $id)->first();
        $v=vendor::where('user_id',$id)->first();
        service::where('user_id',$id)->delete();
        employee::where('salon_id', $user->salon->id)->delete();
        booking::where('salon_id',$user->salon->id)->delete();
        gallery::where('salon_id', $user->salon->id)->delete();
        comment::where('salon_id', $user->salon->id)->delete();
        if(!$user->delete() || !$v->delete() || !$s->delete()){
            session()->flash('message', 'Vendor not deleted Successfully, Try again');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        session()->flash('message', 'Vendor Delete successful');
        session()->flash('type', 'success');
        return redirect()->route('admin.vendor');
    }

    public function vendorSub($id){
        $due_date;
        $v= vendor::find($id);
        if ($v->freq == 'year') {
            $due_date=date('Y-m-d',strtotime(date('Y-m-d').'+1 year'));
        }else{
            $due_date=date('Y-m-d',strtotime(date('Y-m-d').'+1 month'));
        }
        $v->sub_date = date('Y-m-d');
        $v->due_date = $due_date;
        
        if(!$v->save()){
            session()->flash('message', 'Problem occur while updating Subscription, Try again');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        
        session()->flash('message', 'Subscription Updated Successfully');
        session()->flash('type', 'success');
        return redirect()->back();
    }

    public function changeSub(Request $request){
        $this->validate($request, [
            'freq'=>'required'
        ]);

        $v=vendor::find($request->id);
        $v->freq=$request->freq;

        if(!$v->save()){
            session()->flash('message', 'Problem occur while updating Subscription Frequency, Try again');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        session()->flash('message', 'Updating Subscription type Successful');
        session()->flash('type', 'success');
        return redirect()->back();
    }
}
