<?php

namespace App\Http\Controllers;

use App\booking;
use App\comment;
use App\employee;
use App\gallery;
use App\salon;
use App\vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\service;
use App\User;

class salonController extends Controller
{
    //

    public function __construct(){
        $this->middleware('jwt.auth');
    }

    public function reAssign(Request $request){
        $this->validate($request,[
            'reassign_details'=>'required'
        ]);
        $user=auth::user();
        $salon=$user->salon;

        $data=json_decode($request->input('reassign_details'));

        foreach ($data as $dt){
            $booking=booking::find($dt->id);
            if($booking){
                $booking->employee_id=$dt->employee;
                $booking->save();
            }
        }

        return response([
            'status'=>200,
            'message'=>'successfull',
            'data'=>[],
        ],200);

    }

    public function getDashboard(Request $request){
        $user=auth::user();
        $salon=$user->salon;
        $date=$request->query('date');

        $data=[];
        $data['reservation']=0;
        $data['work_hours']=0;
        $data['income']=0;

        $result=booking::where(['salon_id'=>$salon->id,'status'=>2])->whereDate('date',date('Y-m-d',strtotime($date)))->get();



        foreach ($result as $bk){
            $data['income']+=$bk->price;
            $data['work_hours']+=$bk->time_total;
            $data['reservation']+=1;
        }
        $data['work_hours']=ceil($data['work_hours']/60);


        return $data;

    }

    public function respondToComment(Request $request,$id){
        $user=Auth::user();
        $details=$request->input();
        if(!isset($details['text'])){
            return response()->json([
                'status' => false,
                'message'  => 'Empty response is not allowed',
            ]);
        }

        $comt=comment::where(['parent_id'=>$id])->first();
        if($comt){
            return response()->json([
                'status' => false,
                'message'  => 'You can only respond once to this comment',
            ]);
        }

        $comment=new comment();
        $comment->user_id=$user->id;
        $comment->salon_id=$user->salon->id;
        $comment->rating=0;
        $comment->parent_id=$id;
        $comment->text=$request->input('text');
        $comment->save();


        return response()->json([
            'status' => true,
            'message'  => 'Comment was saved successfully',
            'data'  => $comment
        ]);
    }

    public function dashboard(){
        $services=service::where('user_id',Auth::user()->id)->with('salon')->get();
        $salons=salon::where('user_id',Auth::user()->id)->get();
        $employees=employee::where('user_id',Auth::user()->id)->get();
        $photos=gallery::where('salon_id',Auth::user()->salon->id)->get();
        $homeImage=gallery::where(['salon_id'=>Auth::user()->salon->id,'type'=>1])->first();
        $profileImage=gallery::where(['salon_id'=>Auth::user()->salon->id,'type'=>2])->first();
        $featuredImages=gallery::where(['salon_id'=>Auth::user()->salon->id,'type'=>3])->get();

        $salon_ids=[]; foreach ($salons as $salon){$salon_ids[]=$salon->id;}
        $bookings=booking::whereIn('salon_id',$salon_ids)->where('status',0)->with('customer')->get();
        $reservations=booking::whereIn('salon_id',$salon_ids)->where('status','>',0)->with('customer')->get();
        $collect=collect($reservations);
        $reservations=$collect->groupBy('date');
//        foreach ($reservations as $r){
//            dd($r);
//        }


        return view('salon.biz_admin')->with([
            'services'=>$services,
            'salons'=>$salons,
            'employees'=>$employees,
            'photos'=>$photos,
            'homeImage'=>$homeImage,
            'profileImage'=>$profileImage,
            'featuredImages'=>$featuredImages,
            'booking' => $bookings,
            'groups'=>$reservations
        ]);
    }

    public function place_order(Request $request){
        session()->flash('booking','pass');
        $order=$request->input();



        if($order['date']== '' || !isset($order['date'])){
            $this->setAlert(null,'danger','Please pick a date');

            return redirect()->back()->with($order);
        }
        if($order['time']== '' || !isset($order['time'])){
            $this->setAlert(null,'danger','Please pick a time');

            return redirect()->back()->with($order);
        }
        if(!isset($order['service'])){
            $this->setAlert(null,'danger','Please pick a service');

            return redirect()->back()->with($order);
        }
        if(!isset($order['salon'])){
            $this->setAlert(null,'danger','Please pick a salon');
            return redirect()->back()->with($order);
        }


        $book=new booking();
        $book->user_id=Auth::user()->id;
        $book->salon_id=$order['salon'];
        $book->employee_id=0;

        $book->user_name=$order['name'];

        $book->date=$order['date'];
        $book->time=$order['time'];
        $book->note=null;
        $book->status=0;


        $serv=service::find($order['service']);
        $details=[
            'id' => $serv->id,
            'name'  => $serv->name,
            'price '    =>$serv->price
        ];
        $book->price=$serv->price;
        $book->services=json_encode([$details]);
        $book->save();

//        dd($order);

        $this->setAlert(null,'success','Your appointment was successfully.');
        return redirect()->back();
    }

    public function manage_bookings($id){
        session()->flash('booking','pass');
        $book=booking::where('id',$id)->with('salon','salon.owner')->first();

        if(!$book){
            return redirect()->back();
        }
        if(Auth::user()->id != $book->salon->owner->id){
            return redirect()->back();
        }



        if($id == 1){
            $book->status='1';
            $book->save();
            $this->setAlert(null,'success','Reservation was accepted');
        }else if($id ==2){
            $book->status='2';
            $book->save();
            $this->setAlert(null,'danger','Reservation has been rejected');
        }


        return redirect()->back();
    }


    public function add_service(Request $request){

        $this->validate($request,[
            'name'=>'required',
            'salon'=>'required',
            'description'=>'required',
            'price'=>'required',
            'time'=>'required',
            'featured' =>'required',
            'action' =>'required'
        ]);

        $details=$request->input();

        $User=auth::user($request->query('token'));

        $service=new Service();
        $service->name=$details['name'];
        $service->user_id=$User->id;
        $service->salon_id=$details['salon'];
        $service->description=$details['description'];
        $service->price=$details['price'];
        $service->time=$details['time'];
        $service->featured=$details['featured'] ? 1:0;
        $service->action=$details['action'] ? 1:0;
        $service->save();

        return response()->json([
            'status' => true,
            'message'  => 'Service was added successfully',
            'data'  => $service
        ]);
    }
    public function update_service(Request $request,$id){

        $this->validate($request,[
            'name'=>'required',
            'description'=>'required',
            'price'=>'required',
            'time'=>'required',
            'featured' =>'required',
            'action' =>'required'
        ]);

        $details=$request->input();


        $User=auth::user($request->query('token'));

        $service=service::where(['user_id'=>$User->id,'id'=>$id])->first();
        if(!$service){
            return response(
                [
                    'status'    => false,
                    'message'   => 'Service was not found',
                ],404
            );
        }

        $service->name=$details['name'];
        $service->description=$details['description'];
        $service->price=$details['price'];
        $service->time=$details['time'];
        $service->featured=$details['featured'] ? 1:0;
        $service->action=$details['action'] ? 1:0;
        $service->save();

        return response()->json(
            [
                'status'    => true,
                'message'   => 'Service was updated successfully',
                'data'      => $service
            ]
        );
    }
    public function delete_service(Request $request,$id){

        $User=auth::user($request->query('token'));

        $service=service::where(['user_id'=>$User->id,'id'=>$id])->first();
        if(!$service){
            return response()->json([
                'status' => true,
                'message'  => 'Service not found',
            ],404);
        }

        $service->delete();

        return response()->json([
            'status' => true,
            'message'  => 'Service was removed successfully',
            'data'  => $service
        ]);
    }


    public function add_salon(Request $request){

        $this->validate($request,[
            'name'=>'required',
            'contact'=>'required',
            'address'=>'required',
            'city'=>'required',
        ]);

        $details=$request->input();
        $User=auth::user($request->query('token'));


        $check=salon::where('name',$details['name'])->first();
        if($check && $check->user_id != $User->id){
            return response()->json([
                'status' => false,
                'message'  => 'Salon name already exist.',
            ]);
        }



        $salon=salon::where('user_id',$User->id)->first();
        if(!$salon){
            $salon=new salon();
        }
        $salon->name=$details['name'];
        $salon->user_id=$User->id;
        $salon->contact=$details['contact'];
        $salon->city=$details['city'];
        $salon->description=$details['description'];
        $salon->address=$details['address'];
        $salon->salon_type=$details['salon_type'];

        $salon->payment_methods=$details['payment_methods'];
        $salon->map=$details['map'];
        $salon->facebook=$details['facebook'];
        $salon->instagram=$details['instagram'];
        $salon->website=$details['website'];
        $salon->home_service=$details['home_service'] ? 1:0;
        $salon->wifi=$details['wifi'] ? 1:0;
        $salon->parking=$details['parking'] ? 1:0;
        $salon->inviting_persons=$details['inviting_persons'] ? 1:0;

        $salon->save();

        return response()->json([
            'status' => true,
            'message'  => 'Salon updated',
            'data'  => $salon
        ]);
    }
    public function update_salon(Request $request,$id){
        $request->session()->flash('edit-salon','pass');
        $request->session()->flash('salon','pass');

        $this->validate($request,[
            'name'=>'required',
            'contact'=>'required',
            'address'=>'required',
            'working_hours'=>'required',
        ]);

        $details=$request->input();


        $salon=salon::where(['user_id'=>Auth::user()->id,'id'=>$id])->first();
        if(!$salon){
            return response(
                [
                    'status'    => false,
                    'message'   => 'Salon was not found',
                ],404
            );
        }

        $salon->name=$details['name'];
        $salon->contact=$details['contact'];
        $salon->working_hours=$details['working_hours'];
        $salon->address=$details['address'];

        $salon->payment_methods=$details['payment'];
        $salon->map=$details['map'];
        $salon->facebook=$details['facebook'];
        $salon->instagram=$details['instagram'];
        $salon->website=$details['website'];
        $salon->home_service=isset($details['home_service']) ?1:0;
        $salon->wifi=isset($details['wifi']) ?1:0;
        $salon->parking=isset($details['parking']) ? 1 : 0;
        $salon->inviting_persons=isset($details['inviting_persons']) ?1:0;


        $salon->save();

        $request->session()->remove('edit-salon');
        $this->setAlert($request,'success',$salon->name.' has been updated.');
        return redirect()->back();
    }
    public function delete_salon($id){
        session()->flash('salon','pass');
        $salon=salon::where(['user_id'=>Auth::user()->id,'id'=>$id])->first();

        if(!$salon){
            return redirect()->back();
        }

        $salon->delete();
        $services=service::where(['user_id'=>Auth::user()->id,'salon_id'=>$id])->delete();

        session()->flash('type','success');
        session()->flash('message',$salon->name .' salon was deleted successfully');

        return redirect()->back();
    }


    public function add_employee(Request $request){

        $this->validate($request,[
            'name'=>'required',
            'salon'=>'required',
            'contact'=>'required',
            'place'=>'required',
            'services'=>'required',
        ]);

        $details=$request->input();

        $User=auth::user($request->query('token'));

        $employee=new employee();
        $employee->name=$details['name'];
        $employee->user_id=$User->id;
        $employee->salon_id=$details['salon'];
        $employee->contact=$details['contact'];
        $employee->level=1;
        $employee->place=$details['place'];
        $employee->services=$details['services'];

        $employee->save();

        return response()->json(
            [
                'status'    => true,
                'message'   => 'Employee was added successfully',
                'data'      => $employee
            ]
        );
    }
    public function update_employee(Request $request,$id){
        $this->validate($request,[
            'name'=>'required',
            'contact'=>'required',
            'place'=>'required',
            'services'=>'required',
        ]);

        $details=$request->input();

        $User=auth::user($request->query('token'));

        $employee=employee::where(['user_id'=>Auth::user()->id,'id'=>$id])->first();
        if(!$employee){
            return response(
                [
                    'status'    => false,
                    'message'   => 'Employee was not found',
                ],404
            );
        }

        $employee->name=$details['name'];
        $employee->contact=$details['contact'];
        $employee->place=$details['place'];
        $employee->services=$details['services'];
        $employee->save();

        return response()->json(
            [
                'status'    => true,
                'message'   => 'Employee was updated successfully',
                'data'      => $employee
            ]
        );
    }
    public function delete_employee(Request $request,$id){
        $User=auth::user($request->query('token'));

        $employee=employee::where(['user_id'=>$User->id,'id'=>$id])->first();

        if(!$employee){
            return response(
                [
                    'status'    => false,
                    'message'   => 'Employee was not found',
                ],404
            );
        }

        $employee->delete();


        return response()->json(
            [
                'status'    => true,
                'message'   => 'Employee was removed successfully',
                'data'      => $employee
            ]
        );
    }


    public function update_user(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'contact'=>'required',
        ]);

        $details=$request->input();

        $user=auth::user($request->query('token'));
        if(!$user){
            return response(
                [
                    'status'    => false,
                    'message'   => 'Vendor account was not found',
                ],404
            );
        }

        $check=User::where('name',$details['name'])->where('id','!=',$user->id)->first();

        if($check){
            return response(
                [
                    'status'    => false,
                    'message'   => 'Username already exist',
                ],404
            );
        }



        $user->name=$details['name'];
        $user->contact=$details['contact'];
        if(isset($details['whatsapp'])){
            $user->whatsapp=$details['whatsapp'];
        }
        $user->address=isset($details['address']) ? $details['address']:'';

        $user->receive_notification=isset($details['receive_notification']) ? $details['receive_notification']:null;


        $vendor=vendor::where('user_id',$user->id)->first();
        if($vendor){
            $vendor->phone=$details['contact'];
            $vendor->first_name=$details['first_name'];
            $vendor->last_name=$details['last_name'];
            $vendor->username=$details['name'];
            $vendor->save();
        }

        if(!empty($details['password'])){
            if(password_verify($details['current_password'],$user->password)){
                $user->password=bcrypt($details['password']);
            }else{
                return response(
                    [
                        'status'    => false,
                        'message'   => 'Invalid password',
                    ],200
                );
            }
        }

        $user->save();
        $user=User::find($user->id);



        return response()->json(
            [
                'status'    => true,
                'message'   => 'Profile updated successfully',
                'data'      => $user
            ]
        );
    }


    public function upload_picture(Request $request){

        $User=auth::user($request->query('token'));

        if(count($User->salon->images) >= 20){
            return response(
                [
                    'status'    => false,
                    'message'   => 'You have reached your upload limit (20)',
                ],404
            );
        }


        if($request->file() > 0){
            foreach ($request->file() as $key => $file){
                $fileformats=['jpg','JPG','jpeg','JPEG','png','PNG'];

                if(in_array($file->getClientOriginalExtension(),$fileformats)){
                    $filename=date('ymdhis').mt_rand(1,9999999).'.'.$file->getClientOriginalExtension();
                    $file->move(storage_path('/gallery'),$filename);

                    $photo=new gallery();
                    $photo->salon_id=$User->salon->id;
                    $photo->filename='http://'.$request->getHttpHost().'/salon-dashboard/view-gallery/'.$filename;
                    // $photo->filename=$filename;
                    $photo->save();
                }
            }
            $User=User::find($User->id);
            return response()->json(
                [
                    'status'    => true,
                    'message'   => 'Image was added successfully',
                    'data'      => $User
                ]
            );
        }

        return response(
            [
                'status'    => false,
                'message'   => 'You uploaded no image',
            ],404
        );
    }
    public function delete_picture(Request $request,$id){
        $User=auth::user($request->query('token'));

        $photo=gallery::where(['id'=>$id,'salon_id'=>$User->salon->id])->first();
        if($photo){
            $photo->delete();
        }

        $User=User::find($User->id);
        return response()->json(
            [
                'status'    => true,
                'message'   => 'Image was deleted successfully',
                'data'      => $User
            ]
        );
    }
    public function set_home_picture(Request $request,$id){
        $User=auth::user($request->query('token'));

        $photo=gallery::where(['salon_id'=>$User->salon->id,'id'=>$id])->first();
        if(!$photo){
            return response(
                [
                    'status'    => false,
                    'message'   => 'no image',
                ],404
            );
        }

        $home_photo=gallery::where(['salon_id'=>$User->salon->id,'type'=>1])->first();
        if($home_photo){
            $home_photo->type=0;
            $home_photo->save();
        }

        $photo->type=$photo->type == 1 ? 0:1;
        $photo->save();

        $User=User::find($User->id);
        return response()->json(
            [
                'status'    => true,
                'message'   => 'Image was update successfully',
                'data'      => $User
            ]
        );
    }
    public function set_profile_picture(Request $request,$id){
        $User=auth::user($request->query('token'));

        $photo=gallery::where(['salon_id'=>$User->salon->id,'id'=>$id])->first();
        if(!$photo){
            return response(
                [
                    'status'    => false,
                    'message'   => 'no image',
                ],404
            );
        }

        $profile_picture=gallery::where(['salon_id'=>$User->salon->id,'type'=>2])->first();
        if($profile_picture){
            $profile_picture->type=0;
            $profile_picture->save();
        }

        $photo->type=$photo->type == 2 ? 0:2;
        $photo->save();

        $User=User::find($User->id);
        return response()->json(
            [
                'status'    => true,
                'message'   => 'Image was update successfully',
                'data'      => $User
            ]
        );
    }
    public function set_featured_picture(Request $request,$id){
        $User=auth::user($request->query('token'));

        $photo=gallery::where(['salon_id'=>$User->salon->id,'id'=>$id])->first();
        if(!$photo){
            return response(
                [
                    'status'    => false,
                    'message'   => 'no image',
                ],404
            );
        }

        $featured_photo=gallery::where(['salon_id'=>$User->salon->id,'type'=>3])->get();
        if(count($featured_photo) >=  6){
           if($photo->type != 3){
               return response(
                   [
                       'status'    => false,
                       'message'   => 'limit',
                   ],404
               );
           }
        }

        $photo->type=$photo->type == 3 ? 0:3;
        $photo->save();

        $User=User::find($User->id);
        return response()->json(
            [
                'status'    => true,
                'message'   => 'Image was update successfully',
                'data'      => $User
            ]
        );
    }

    public function setAlert($request=null,$type,$message){
        session()->flash('message', $message);
        session()->flash('type', $type);
    }

    public function noShowBooking($id){
        $user=Auth::user();

        $booking=booking::where(['id'=>$id,'salon_id'=>$user->id])->first();
        if(!$booking){
            return response(['status'=>false,'message'=>"Booking not found"],200);
        }

        $booking->status=-2;
        $booking->save();

        return response(['status'=>true,'message'=>"Booking has been marked as no show",'data'=>$booking],200);
    }

    public function logout(){
        Auth::logout();

        return redirect()->to('/');
    }
}
