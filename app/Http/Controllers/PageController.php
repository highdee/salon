<?php

namespace App\Http\Controllers;

use App\comment;
use App\employee;
use App\salon;
use App\service;
use App\User;
use App\vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class PageController extends Controller
{

    public $services_ids=[];

    public function salon(Request $request,$name=null){

        $details=$request->input();
        $name=isset($details['salon_name']) ? $details['salon_name']:$name;
        $salon=salon::where(['name'=>$name])->with('services')->with('employees')->with('images')->with('comments')->first();
        $salon->upcomingEvents=$salon->getUpcomingSchedules();
        $vendor=User::find($salon->user_id);

        $rating=$salon->rating();
        $salon->related=$salon->related();
        $salon->comment_count=$salon->comments()->count();

        if(!$salon || !$vendor) {
            return response('',404);
        }

        $services=[];
        foreach ($salon->services as $service){
            $services[]=$service;
        }

        $salon->services=$services;

        if($salon->status != 0){
            return response('',404);
        }

        return response($salon,200);
    }
    public function getSalonComment($id){
        $comments=comment::where(['salon_id'=>$id,'parent_id'=>0])->with('user')->with('response','response.salon')->orderBy('id','desc')->paginate(10);

        return $comments;
    }
    public function salon_list(Request $request){
        $keywords=$request->query('q');
        $result=[];
        if($keywords == null){
            $salons=salon::where('status',0)->with('services')->paginate(10);
            $result=$this->sortServices($salons);
        }else{

        }
    	return view('salon_list')->with([
    	    'salons' => $result
        ]);
    }

    public function suggestion(Request $request){
        $services=service::where('id','>',0)->where('name','like','%'.$request->input('service').'%')->with('salon')->limit(10)->get(['name','salon_id']);
        $salons=salon::where('name','like','%'.$request->input('service').'%')->limit(10)->get(['name']);

        return array_merge(collect($services)->toArray(),collect($salons)->toArray());
    }

    public function getSalonType($type){
        $salons=salon::where('salon_type','like','%'.$type.'%')->paginate();

        return $salons;
    }

    public function getFeaturedSalons(){
        $salons=salon::where(['featured'=>1,'status'=>0])->limit(10)->get();

        return $salons;
    }

    public function search(Request $request){
        $data=$request->input();
        $location=isset($data['location']) ? $data['location']:'';
        $service=isset($data['service']) ? $data['service']:'';
        $paginate=10;

        $salons=[];


        $services=service::where('id','>',0);

        $employees=[];
        if(isset($data['service'])){
            $services=$services->where('name','like','%'.$service.'%');
            $employees=employee::where('place','like','%'.$service.'%')->with('salon')->get();

//            return $employees;
        }
        if ($request->input('price1') != null) {
            $services=$services->where('price','>=',$request->input('price1'));
        }
        if ($request->input('price2') != null) {
            $services=$services->where('price','<=',$request->input('price2'));
        }

        $services=$services->with('salon')->get();


        if(count($services) > 0){
            foreach ($services as $serv){
                $salons[]=$serv->salon_id;
                $this->services_ids[]=$serv->id;
            }
        }

        if(count($employees) > 0){
            foreach ($employees as $employee){
                $salons[]=$employee->salon->id;
            }
        }


        $salons=salon::where('status',0)->whereIn('id',$salons);



        if(isset($data['service'])){
            $salons=$salons->orWhere('name','like','%'.$data['service'].'%')->orWhere('salon_type','like','%'.$service.'%');
        }
        if(isset($data['type'])){
            $salons=$salons->Where('salon_type', 'like', '%' . $data['type'] . '%');
        }


        if(empty($data['service']) && empty($data['type'])){
            $salons=salon::where('status',0)->with('services');
        }

        if(isset($data['featured'])){
            $salons=$salons->Where('featured', 1);
        }

        $salons=$salons->with('images')->with(['services' => function($query){ $query->whereIn('id',$this->services_ids);}]);

        $salons=$salons->paginate($paginate);

        $result=$this->sortServices($salons);

        return response($result,200);

    }

    public function sortServices($salons){
        foreach ($salons as $salon){
          $services=service::where('featured',1)->get();
          $salon->services_all=$services;
        }
        return $salons;
    }




}
