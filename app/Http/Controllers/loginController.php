<?php

namespace App\Http\Controllers;

use App\Mail\forgetPassword;
use App\shift;
use App\User;
use App\vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use JWTAuth;

class loginController extends Controller
{
    //

    public function create_account(Request $request){
        $this->validate($request,[
            'email'=>'required|email|unique:users',
            'name'=>'required|unique:users',
            'password'=>'required'
        ]);

        $details=$request->input();


        $User=new User();
        $User->name=$details['name'];
        $User->email=$details['email'];
        $User->password=bcrypt($details['password']);
        $User->save();



        return response()->json([
            'status'=>true,
            'msg'=>"Your registration was successfull"
        ]);

    }

    public function create_vendor_account(Request $request){

        $this->validate($request,[
            'email'=>'required|email|unique:users',
            'phone'=>'required',
            'person'=>'required',
            'time'=>'required',
            'type'=>'required',
        ]);


        $details=$request->input();


        $User=new User();
        $User->name=$details['name'];
        $User->email=$details['email'];
        $User->contact=$details['phone'];
        $User->password=bcrypt('123');
        $User->save();

        $vendor=new vendor();
        $vendor->user_id=$User->id;
        $vendor->contact_person=$details['person'];
        $vendor->contact_phone=$details['phone'];
        $vendor->salon_type=$details['type'];
        $vendor->contact_time=$details['time'];
        $vendor->save();

        return response()->json([
            'status'=>true,
            'msg'=>"Your registration was successfull"
        ]);
    }


    public function login(Request $request){
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required'
        ]);

        $credentials = $request->only('email', 'password');

        if($token=JWTAuth::attempt($credentials)){
            $user=User::where('id',auth::user($token)->id)->with('salon','salon.employees','salon.employees.booking')->with('vendor')->first();
            if($user->status == -1){
                return response()->json(
                    [
                        "status"=>false,
                        "message"=>"Your account has been disabled."
                    ]
                );
            }
            return response()->json(['token'=>$token,"user"=>$user]);
        }else{
            
            //trying username
            if($token=JWTAuth::attempt(['name'=>$credentials['email'],'password'=>$credentials['password']])){
                $user=User::where('id',auth::user($token)->id)->with('salon','salon.employees','salon.employees.booking')->with('vendor')->first();
                if($user->status == -1){
                    return response()->json(
                        [
                            "status"=>false,
                            "message"=>"Your account has been disabled."
                        ]
                    );
                }
                return response()->json(['token'=>$token,"user"=>$user]);
            }
            return response()->json(
                [
                    "status"=>false,
                    "message"=>"Invalid login details"
                ]
            );
        }
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function forget_password(Request $request){
        $this->validate($request,[
            'email'=>'required', 
        ]);

        $User=User::where('email',$request->input('email'))->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $User->remember_token=mt_rand(99999,99999999).str_random(12).mt_rand(99999,99999999).str_random(12).mt_rand(99999,99999999).str_random(12);
        $User->save();

        $link=env('APP_URL_FRONTEND')."/reset-password/".$User->remember_token;
        Mail::to($User->email)->sendNow(new forgetPassword([
            'user'=>$User->name,
            'link'=> $link
        ]));

        return response()->json([
            'status'=>true,
            'msg'=>"Reset mail has been sent"
        ]);
    }

    public function verify_password(Request $request){
        $token=$request->query('code');
        // $this->validate($request, [
        //     'token'=>'required'
        // ]);
        // $token=$request->token;
        if(!isset($token)){
            return response()->json(['status'=>false,'msg'=>"Token not found" ]);
        }

        $User=User::where('remember_token',$token)->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Expired link"
            ]);
        }

        return response()->json(['status'=>true,'msg'=>"Correct token" ]);
    }

    public function verify_token(Request $request){
        // $token=$request->query('code');
        $this->validate($request, [
            'token'=>'required'
        ]);
        $token=$request->token;
        if(!isset($token)){
            return response()->json(['status'=>false,'msg'=>"Token not found" ]);
        }

        $User=User::where('remember_token',$token)->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Expired link"
            ]);
        }

        return response()->json(['status'=>true,'msg'=>"Link verified, Enter new password" ]);
    }

    public function reset_password(Request $request){
        $this->validate($request,[
            'password'=>'required|confirmed',
            'token'=>'required'
        ]);

        $User=User::where('remember_token',$request->input('token'))->first();

        if(!$User){
            return response()->json([
                'status'=>false,
                'msg'=>"Account not found"
            ]);
        }

        $User->password=bcrypt($request->input('password'));
        $User->remember_token=mt_rand(99999,99999999).str_random(12);
        $User->save();

        return response()->json(['status'=>true,'msg'=>"Password reset was successful" ]);
    }

    public function setAlert($request,$type,$message){
        $request->session()->flash('message', $message);
        $request->session()->flash('type', $type);
    }

    public function getRandomAlphaNumeric(){
        $alpha="ABCDEFGHIJKLMNOPQSRTUVWXYZ";
        return mt_rand(0,999999).$alpha[mt_rand(0,25)].mt_rand(0,9999999).$alpha[mt_rand(0,25)].mt_rand(0,9999999);
    }


}
