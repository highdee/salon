<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use App\vendor;
use App\service;
use App\salon;
use App\employee;
use App\booking;
use App\gallery;
use App\comment;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\addVendorMail;

class adminController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    } 

    public function index(){
        $user=DB::table('users AS u')
            ->select('u.id','u.name','u.contact','u.whatsapp','u.email','u.address','u.status')
            ->leftJoin('vendors', 'vendors.user_id', '=', 'u.id')
            ->whereNull('vendors.user_id')->get()->count();
        // $user=User::get()->count();
        $salon=salon::get()->count();
        $vendor=vendor::get()->count();
        $booking=booking::get()->count();
        //dd($salon);
    	return view('admin.index')->with([
            'user'=>$user,
            'vendor'=>$vendor,
            'salon'=>$salon,
            'booking'=>$booking
        ]);
    }

    public function vendorList(){
        $r = Vendor::all();
        //dd($r[0]->salon);
        return view('admin.vendor')->with('vendors', $r);
    }
    
    public function userList(){
        $u=DB::table('users AS u')
            ->select('u.id','u.name','u.contact','u.whatsapp','u.email','u.address','u.status')
            ->leftJoin('vendors', 'vendors.user_id', '=', 'u.id')
            ->whereNull('vendors.user_id')->get();    
                                                    
        return view('admin.user')->with('users',$u);
    }
    public function user_show($userid){
        $r=User::find($userid);
        $u=booking::where('user_id',$userid)
                    ->where('date', '<', date("Y-m-d"))
                    ->where('status', '!=', 2)
                    ->get();
        return view('admin.user_show')->with(['user'=>$r,'show'=>$u]);  
    }

    public function vendor_show($salonid){
        $r=vendor::where('salon_id',$salonid)->first();
        $s=service::where('salon_id',$salonid)->get(['name','user_id','salon_id','description','price','time','featured','action']);
        $sa=salon::where('id',$salonid)->get();
        $e=employee::where('salon_id',$salonid)->get();

        $total=0;
        $mo_t=0;
        $sm_t=0;
        $yr_t=0;

        for ($i=0; $i < count($sa[0]->booking); $i++) { 
            $total=$total + $sa[0]->booking[$i]->price;
        }
        $month=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-1 month')))
                        ->where('salon_id',$salonid)
                        ->get();
        for ($i=0; $i < count($month); $i++) { 
            $mo_t=$mo_t + $month[$i]->price;
        }
        $six_m=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-6 month')))
                        ->where('salon_id',$salonid)
                        ->get();
        for ($i=0; $i < count($six_m); $i++) { 
            $sm_t=$sm_t + $six_m[$i]->price;
        }
        $year=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-1 year')))
                        ->where('salon_id',$salonid)
                        ->get();
        for ($i=0; $i < count($year); $i++) { 
            $yr_t=$yr_t + $year[$i]->price;
        }
        // dd($sa[0]->booking);
        // $b=booking::where
        // return $s;   
        // $f=[]; 
        // foreach ($sa->employees as $value) {
        //     array_push($f, $value);
        // }
        //return $sa->employee;
        return view('admin.vendor_show')->with([
            'vendors'=>$r,
            'services'=>$s,
            'salon'=>$sa,
            'employee'=>$e,
            'amount'=>$total,
            'fmonth'=>$month, 'smonth'=>$six_m,'year'=>$year,'ftotal'=>$mo_t,'stotal'=>$sm_t,'ytotal'=>$yr_t
        ]);
    }

    public function logout() {
	  Auth::logout();
	  return redirect('/adminLogin');
	}

    public function verify(){
        $r=vendor::where('verified',null)->get();
         //return $r;
        return view('admin.verify')->with('vendors',$r);
    }

    public function verify_show($userid){
        $u = User::where('id',$userid)->first();
        $v = vendor::where('user_id',$userid)->first();

        return view('admin.verify_show')->with([
            'users'=>$u,
            'vendors'=>$v
        ]);
    }

    public function verify_vendor(Request $request){
        $this->validate($request,[
            'password'=>'required'
        ]);
        $u = User::where('id', $request->userid)->first();

        $u->password = Hash::make($request->password);
        

        $v=vendor::where('user_id', $request->userid)->first();
        $v->verified = now();
        
        $r=vendor::where('verified',null)->get();
        if ($u->save() && $v->save()) {
            return view('admin.verify')->with([
                'vendors'=>$r,
                'successMsg'=>'Vendor Verified Successfully'
            ]);
        }
        
        return redirect()->back()->with([
            
            'errorMsg'=>'Error Verifying Vendor, Try again Later'
        ]);
    }
    public function addVendor(Request $request){
        $this->validate($request, [
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'company_name'=>'required|string',
            'username'=>'required|string',
            'company_name'=>'required|string',
            'salon_name'=>'required|string',
            'email'=>'required|email|unique:users,email',
            'sub_date'=>'required',
            'freq'=>'required',
            'phone'=>'required',
            'password'=>'required',
        ]);   
        $due_date;
        if ($request['sub_date'] == 'year') {
            $due_date=date('Y-m-d',strtotime($request['sub_date'].'+1 year'));
        }else{
            $due_date=date('Y-m-d',strtotime($request['sub_date'].'+1 month'));
        }
         
        $u = new User();
        $u->name=$request['first_name'].' '.$request['last_name'] ;
        $u->email=$request['email'];
        $u->password=Hash::make($request['password']);
        $u->contact = $request['phone'];
        $u->save();

        $v = new vendor();
        $v->first_name=$request['first_name'];
        $v->last_name=$request['last_name'];
        $v->username=$request['username'];
        $v->company_name=$request['company_name'];
        $v->phone = $request['phone'];
        $v->user_id=$u->id;
        $v->sub_date=$request['sub_date'];
        $v->freq=$request['freq'];
        $v->due_date=$due_date;
        $v->verified = now();


        $salon=new salon();
        $salon->name=$request['salon_name'];
        $salon->user_id = $u->id;
        $salon->save();

        $v->salon_id=$salon->id;
        $v->save();

        $payload=[
            'fname'=>$request['first_name'],
            'lname'=>$request['last_name'],
            'email'=>$request['email'],
            'username'=>$request['username'],
            'company'=>$request['company_name'],
            'phone'=>$request['phone'],
            'salon'=>$request['salon_name'],
            'sub_date'=>$request['sub_date'],
            'sub_type'=>$request['freq'],
            'password'=>$request['password']
        ];
        // Mail::to($User->email)->sendNow(new forgetPassword($payload));

        Mail::to($request['email'])->sendNow(new addVendorMail($payload));

        session()->flash('message', 'Vendor Saved Successfully');
        session()->flash('type', 'success');
        return redirect()->back();
    }
    public function salon(){
        $a = salon::all();
        //dd($a[0]) ;
        return view('admin.salon')->with('salons', $a);
    }
    public function salon_show($id){
        $a = salon::find($id);
        //dd ($a->comments[0]->User->name);
        // $b=json_decode($a->booking[0]->services);
        // dd($a->booking[0]->customer->name);
        // dd($b->name);
        // dd(count($a->booking));
        $total=0;
        $mo_t=0;
        $sm_t=0;
        $yr_t=0;
        for ($i=0; $i < count($a->booking); $i++) { 
            $total=$total + $a->booking[$i]->price;
        }
        $month=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-1 month')))
                        ->where('salon_id',$id)
                        ->get();
        for ($i=0; $i < count($month); $i++) { 
            $mo_t=$mo_t + $month[$i]->price;
        }
        $six_m=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-6 month')))
                        ->where('salon_id',$id)
                        ->get();
        for ($i=0; $i < count($six_m); $i++) { 
            $sm_t=$sm_t + $six_m[$i]->price;
        }
        $year=booking::where('date', '>=', date('Y-m-d',strtotime(date('Y-m-d').'-1 year')))
                        ->where('salon_id',$id)
                        ->get();
        for ($i=0; $i < count($year); $i++) { 
            $yr_t=$yr_t + $year[$i]->price;
        }
        return view('admin.salon_show')->with(['allBooking'=>0,'salon'=>$a,'amount'=>$total, 'fmonth'=>$month, 'smonth'=>$six_m,'year'=>$year,'ftotal'=>$mo_t,'stotal'=>$sm_t,'ytotal'=>$yr_t]);
    }
    public function setFeatured(Request $request){
        $get = salon::find($request['salon_id']);
        $get->featured = !$get->featured;
        if(!$get->save()){
            session()->flash('message', 'Featured not set');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        // $this->salon();
        session()->flash('message', 'Featured set successful');
        session()->flash('type', 'success');
        return redirect()->back();
        
    }

    public function deleteUser($id){
        $user = User::find($id);
        if(!$user->delete()){
            session()->flash('message', 'User not deleted Successfully');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        session()->flash('message', 'User Delete successful');
        session()->flash('type', 'success');
        return redirect()->route('admin.user');
    }

    public function banUser(Request $request){
        $u = $this->validate($request,[
            'user_id'=>'required'
        ]);
        $user=User::find($u['user_id']);
        $user->status = -($user->status) - 1;
        if(!$user->save()){
            session()->flash('message', 'User not Ban, Pls Try Again');
            session()->flash('type', 'error');
            return redirect()->back();   
        }
        session()->flash('message', 'Successfully');
        session()->flash('type', 'success');
        return redirect()->back();
    }
    
    public function banVendor(Request $request){
        $u = $this->validate($request,[
            'user_id'=>'required'
        ]);
        $user=User::find($u['user_id']);
        
        $user->status = -($user->status) - 1;
        $v=vendor::where('user_id',$u['user_id'])->first();
        $v->status = 1 - $v->status;
        if(!$user->save() || !$v->save()){
            session()->flash('message', 'Vendor not Ban, Pls Try Again');
            session()->flash('type', 'error');
            return redirect()->back();   
        }
        session()->flash('message', 'Successfully');
        session()->flash('type', 'success');
        return redirect()->back();
    }
    public function deleteVendor($id){
        $user = User::find($id);
        $s=salon::where('user_id', $id)->first();
        $v=vendor::where('user_id',$id)->first();
        service::where('user_id',$id)->delete();
        employee::where('salon_id', $user->salon->id)->delete();
        booking::where('salon_id',$user->salon->id)->delete();
        gallery::where('salon_id', $user->salon->id)->delete();
        comment::where('salon_id', $user->salon->id)->delete();
        if(!$user->delete() || !$v->delete() || !$s->delete()){
            session()->flash('message', 'Vendor not deleted Successfully, Try again');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        session()->flash('message', 'Vendor Delete successful');
        session()->flash('type', 'success');
        return redirect()->route('admin.vendor');
    }

    public function vendorSub($id){
        $due_date;
        $v= vendor::find($id);
        if ($v->freq == 'year') {
            $due_date=date('Y-m-d',strtotime(date('Y-m-d').'+1 year'));
        }else{
            $due_date=date('Y-m-d',strtotime(date('Y-m-d').'+1 month'));
        }
        $v->sub_date = date('Y-m-d');
        $v->due_date = $due_date;
        
        if(!$v->save()){
            session()->flash('message', 'Problem occur while updating Subscription, Try again');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        
        session()->flash('message', 'Subscription Updated Successfully');
        session()->flash('type', 'success');
        return redirect()->back();
    }

    public function changeSub(Request $request){
        $this->validate($request, [
            'freq'=>'required'
        ]);

        $v=vendor::find($request->id);
        $v->freq=$request->freq;

        if(!$v->save()){
            session()->flash('message', 'Problem occur while updating Subscription Frequency, Try again');
            session()->flash('type', 'error');
            return redirect()->back();
        }
        session()->flash('message', 'Updating Subscription type Successful');
        session()->flash('type', 'success');
        return redirect()->back();
    }
}
