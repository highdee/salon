<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $appends=['vendor','salon'];

    public function getVendorAttribute(){
        return $this->vendor()->first();
    }

    public function getSalonAttribute(){
        return $this->salon()->first();
    }

    public function vendor(){
        return $this->hasOne('App\vendor','user_id','id');
    } 

    public function salon(){
        return $this->belongsTo('App\salon','id','user_id');
    }
    public function booking(){
        return $this->hasMany('App\booking','user_id','id');
    }
    public function salonFavorite(){
        return $this->belongsToMany('App\salon','user_salon','user_id','salon_id');
    }

}
