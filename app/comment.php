<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    //

    protected $appends=['response','user'];


    public function getUserAttribute(){
        return $this->user()->first();
    }

    public function getResponseAttribute(){
        return $this->response()->get();
    }

    public function user(){
        // return $this->belongsTo('App\user');
        return $this->belongsTo('App\User');
    }

    public function response(){
        return $this->hasMany('App\comment','parent_id','id');
    }

    public function salon(){
        return $this->belongsTo('App\salon','salon_id','id');
    }
}
