<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class schedule extends Model
{
    //


    protected $appends=['shift'];

    public function getShiftAttribute(){
        return $this->shift()->first();
    }

    public function shift(){
        return $this->belongsTo('App\shift','shift_id','id');
    }
}
