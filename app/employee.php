<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    protected  $appends=[];

    public function mybooking(){
        return $this->booking()->where('status',0)->orWhere('status',1)->get();
    }

    public function salon(){
    	return $this->belongsTo('App\salon','salon_id','id');
    }

    public function booking(){
        return $this->hasMany('App\booking','employee_id','id')->where('status',0)->orWhere('status',1);
    }
}
