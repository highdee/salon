<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class service extends Model
{
    //

    protected $appends=['type'];

    public function getTypeAttribute(){
        return 'service';
    }

    public function salon(){
        return $this->belongsTo('App\salon','salon_id','id');
    }
}
