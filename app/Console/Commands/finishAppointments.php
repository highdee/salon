<?php

namespace App\Console\Commands;

use App\booking;
use App\Mail\notification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class finishAppointments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:appointments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Europe/Zagreb");
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $bookings=booking::where(['status'=>1])->Orwhere(['status'=>0])->whereDate('date','<=',date('Y-m-d'))->with('customer')->get();

        foreach ($bookings as $book){
            $total_time=$book->time_total + 4;

            $booking_ends=date('H:i',strtotime($book->time.'+'.$total_time.' minutes'));
            $current_time=(date('H')+1).':'.date('i');



            if($book->status == 1){
                //SEND NOTIFICATION FOR REMINDER
                $diff2=date_diff(date_create($book->date.' '.$book->time),date_create(date('Y-m-d').' '.$current_time));
                if($book->notified == 0 && $diff2->i < 60 && $diff2->h == 0 && $diff2->d == 0 && $diff2->invert == 1){
                    $data=[
                        'user'=> $book->customer->name,
                        'text'=>" <h4 style='color:green'>This is a reminder towards your appointment with ".$book->salon()->first()->name." </h4>
                        <table>
                                <tr>
                                         <td>Service:</td><td>".$book->service_names."</td>
                                 </tr>
                                      <tr><td>Employee:</td><td>".$book->employee->name."</td></tr>
                                         <tr><td>Date:</td><td>".$book->date."</td></tr>
                                         <tr><td>Time:</td><td>".$book->time."</td></tr>
                                         <tr><td>Price:</td><td>".$book->price." kn</td></tr>               
                                  
                        </table>
                        <br>  
                     "
                    ];
                    Mail::to($book->customer()->first()->email)->send(new notification($data));

                    $book->notified=1;
                    $book->save();
                }


                //if appointment has been completed
                $diff=date_diff(date_create($book->date.' '.$booking_ends),date_create(date('Y-m-d').' '.$current_time));
                if(date('Y-m-d') == $book->date && ($diff->d == 0 && $diff->i >= 0 && $diff->invert == 0 ) ){
                    $link=env('APP_URL_FRONTEND')."/salon/".$book->salon->name;
                    $data=[
                        'user'=> $book->customer->name,
                        'text'=>" <h4 style='color:green'>Your appointment with ".$book->salon()->first()->name." has been completed.</h4>
                    <table>
                            <tr>
                                     <td>Service:</td><td>".$book->service_names."</td>
                             </tr>
                                  <tr><td>Employee:</td><td>".$book->employee->name."</td></tr>
                                     <tr><td>Date:</td><td>".$book->date."</td></tr>
                                     <tr><td>Time:</td><td>".$book->time."</td></tr>
                                     <tr><td>Price:</td><td>".$book->price." kn</td></tr>

                    </table>
                    <br>
                    <p><a href=".$link." class='btn btn-warning'>GIVE A COMMENT</a></p>
                     "
                    ];
                    Mail::to($book->customer()->first()->email)->send(new notification($data));


                    $book->status=2;
                    $book->save();
//
                }


                //CANCEL BOOKING IF THE TIME HAS PASS AND IT WASN'T MARKED 'NO SHOW'
                $diff3=date_diff(date_create($book->date.' '.$booking_ends),date_create(date('Y-m-d').' '.$current_time));
                if($book->status == 1 && date('Y-m-d') >= $book->date && ($diff3->i > 0 || $diff3->h > 0)  && ($diff3->invert == 0 )){
                    $book->status = -1;
                    $book->save();
                }
            }
            else{
                $diff = date_diff(date_create($book->date),date_create(date('Y-m-d')))->days;
                if($diff > 0 || date('H') > $booking_ends) {
                    $book->deleted_at = date('Y-m-d');
                    $book->status = -1;
                    $book->save();
                }
            }


        }
    }
}
