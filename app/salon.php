<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salon extends Model
{
    //

    public $services_all=[];

    public $appends=['comment_count','employees','services','all_services','type','favorites','images','rate_count','rate_real_count','rates','rating','services_all','working_hours','shifts'];


    protected function getCommentCountAttribute(){
        return $this->comments()->count();
    }

    protected function getAllServicesAttribute(){
        return $this->services()->get();
    }


    protected function getWorkingHoursAttribute(){
        return $this->working_hours()->get();
    }

    protected function getShiftsAttribute(){
        return $this->shifts()->get();
    }

    public function setServicesAllAttribute($value){
        $this->services_all=$value;
    }
    public function getServicesAllAttribute(){
        return $this->services_all;
    }

    public function getFavoritesAttribute(){
        return $this->favorites()->count();
    }
    public function getRateCountAttribute(){
        return $this->rate_count()->count();
    }
    public function getRateRealCountAttribute(){
        return $this->rating()[2];
    }

    public function getRatesAttribute(){
        return $this->rating()[1];
    }

    public function getRatingAttribute(){
        return $this->rating()[0];
    }
    public function getTypeAttribute(){
        return 'salon';
    }
    public function getEmployeesAttribute(){
        return $this->employees()->get();
    }

    public function getServicesAttribute(){
        return $this->services()->get();
    }
    public function getImagesAttribute(){
        return $this->images()->get();
    }

    public function services(){
        return $this->hasMany('App\service','salon_id','id');
    }

    public function employees(){
        // return $this->hasMany('App\employee','salon_id','id');
        return $this->hasMany(employee::class);
    }
    public function owner(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public function profileImage(){
        return gallery::where(['salon_id' => $this->id,'type'=>1])->first();
    }
    public function featuredImages(){
        return gallery::where(['salon_id' => $this->id,'type'=>3])->get();
    }
    public function images(){
        return $this->hasMany(gallery::class);
    }
    public function related(){
        return salon::where(['status'=>0])->where('salon_type','like','%'.$this->salon_type.'%')->where('name','!=',$this->name)->limit(4)->get();
    }
    public function favorites(){
        return $this->belongsToMany('App\User','user_salon','salon_id','user_id');
    }
    public function comments(){
        return $this->hasMany('App\comment','salon_id','id');
    }
    public function booking(){
        return $this->hasMany('App\booking','salon_id','id');
    }
    public function rating(){
        $rate=0;
        $sum=0;
        $comments=$this->comments()->get();
        $count=$this->comments()->where('rating','>',0)->count();;
        $rates=[
            '0'=>0,
            '1'=>0,
            '2'=>0,
            '3'=>0,
            '4'=>0,
            '5'=>0,
        ];
        foreach ($comments as $comment){
            $sum+=$comment->rating;
            $rates[$comment->rating]+=1;
        }
        $rate=$count == 0 ? 0:$sum/$count;

        return [$rate,$rates,$count];
    }
    public function rate_count(){
        return $this->hasMany('App\comment','salon_id','id')->where('rating','>',0);
    }

    public function working_hours(){
        return $this->hasMany('App\workinghours','salon_id','id');
    }

    public function shifts(){
        return $this->hasMany('App\shift','salon_id','id');
    }

    public function upcomingSchedules(){
        $schedules=$this->hasMany('App\schedule','salon_id','id');
        return $schedules;
    }

    public function getUpcomingSchedules(){
        return $this->upcomingSchedules()->where('week','>=',date('W'))->get();

    }
}
