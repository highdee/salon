<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class booking extends Model
{

    /*
     * STATUS DETAILS
     * -1 => No show
     * -1 => Rejected
     * 0 => Booked
     * 1 => Accepted
     * 2 => Completed
     *
     */

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        date_default_timezone_set("Europe/Zagreb");
    }


    protected $appends=['employee','services_data',"price","time_total",'service_names','customer','datee','active','noshow'];

    public function getActiveAttribute(){
        $date_check=$this->date == date('Y-m-d');
        $hour=date('H:i',strtotime($this->time.'+'.$this->time_total.' minutes'));
        $current_time=(date('H')+1).':'.date('i');

        $diff=date_diff(date_create(date('Y-m-d').' '.$current_time),date_create($this->date.' '.$hour));

//        return $diff;
        return $this->status == 1 && $diff->invert == 0 && $diff->d == 0 && $diff->h == 0 && $diff->i < $this->time_total;
    }

    public function getDateeAttribute(){
        return date('d/m/Y',strtotime($this->date));
    }

    public function getCustomerAttribute(){
        return $this->customer()->first();
    }
    public function getEmployeeAttribute(){
        return $this->employee()->first();
    }
    public function  getNoshowAttribute(){
        $user=$this->customer()->first();
        $booking=booking::where(['user_id'=>$user->id,'status'=>-2])->count();
        return $booking;
    }

    public function customer(){
        return $this->belongsTo("App\User",'user_id','id');
    }

    public function salon(){
        return $this->belongsTo('App\salon','salon_id','id');
    }

    public function employee(){
        return $this->belongsTo('App\employee','employee_id','id');
    }

    public function getServicesDataAttribute(){

        return $this->services_data();
    }

    public function services_data(){
        $services=json_decode($this->services);
        return $services;
    }

    public function getPriceAttribute(){
        $price=0;
        foreach ($this->services_data() as $service){
            $price+=$service->price;
        }
        return $price;
    }

    public function getTimeTotalAttribute(){
        $time=0;
        foreach ($this->services_data() as $service){
            $time+=$service->time;
        }
        return $time;
    }
    public function getServiceNamesAttribute(){
        $name='';
        foreach ($this->services_data() as $service){
            $name=$name.','.$service->name;
        }
        return substr($name,1);
    }


}
