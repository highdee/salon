<?php

Auth::routes();


Route::prefix('admin')->group(function(){
	Route::get('/', 'adminController@index')->name('admin.dashboard');
	Route::get('/adminLogout', 'adminController@logout');
	Route::get('/vendorList', 'adminController@vendorList')->name('admin.vendor');
	Route::get('/userList', 'adminController@userList')->name('admin.user');
	Route::get('/user_show/{userid}', 'adminController@user_show');
	Route::get('vendor_show/{userid}', 'HomeController@vendor_show');
	Route::get('verify', 'adminController@verify');
	Route::get('salon', 'adminController@salon');
	Route::get('salon_show/{id}', 'HomeController@salon_show');
    Route::post('verify', 'adminController@verify_vendor');
    Route::get('/verify_show/{userid}', 'adminController@verify_show');
	Route::post('adminAddVendor', 'HomeController@addVendor');
	Route::post('admin/setFeatured', 'adminController@setFeatured');
	Route::get('/deleteUser/{userid}', 'adminController@deleteUser');
	Route::post('/banUser', 'adminController@banUser');
	Route::get('/deleteVendor/{id}', 'adminController@deleteVendor');
	Route::post('/banVendor', 'adminController@banVendor');
	Route::get('/vendorSub/{id}', 'adminController@vendorSub');
	Route::post('/changeSub', 'adminController@changeSub');
});


Route::get('/adminLogin', 'adminLoginController@show');
Route::post('adminLogin', 'adminLoginController@login');
Route::get('/forgot-password', 'PageController@index');


Route::get('/reset-password/{token}', 'loginController@reset_password');
Route::get('/reset-password', 'PageController@index');




 Route::prefix('salon-dashboard')->group(function(){
    Route::get('/view-gallery/{name}', function($name){
		 $imagePath=storage_path().'/gallery/'.$name;
		 return response()->download($imagePath);
	});
 });


Route::get('/logout', 'salonController@logout');
