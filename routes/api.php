<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function(){

    Route::post('login','loginController@login');
    Route::post('vendor-signup','loginController@create_vendor_account');
    Route::post('signup','loginController@create_account');
    Route::post('forgot-password','loginController@forgot_password');
    Route::post('verify-token','loginController@verify_token');
    Route::post('reset-password','loginController@reset_password');

    Route::post('update-salon','salonController@add_salon');
    Route::post('add-employee','salonController@add_employee');
    Route::post('update-employee/{id}','salonController@update_employee');
    Route::get('delete-employee/{id}','salonController@delete_employee');

    Route::post('update-vendor','salonController@update_user');
    Route::post('update-user','salonController@update_user');

    Route::post('add-service','salonController@add_service');
    Route::post('update-service/{id}','salonController@update_service');
    Route::get('delete-service/{id}','salonController@delete_service');

    Route::post('add-image','salonController@upload_picture');
    Route::get('delete-image/{id}','salonController@delete_picture');
    Route::get('set-home-image/{id}','salonController@set_home_picture');
    Route::get('set-profile-image/{id}','salonController@set_profile_picture');
    Route::get('set-featured-image/{id}','salonController@set_featured_picture');
    Route::get('no-show-booking/{id}','salonController@noShowBooking');
    Route::get('get-booking-head/{date}','userController@getBoookingHead');

    Route::get('get-user','userController@getUser');

    Route::get('check-salon-favorites/{id}/{salon}','userController@checkFavorite');
    Route::get('get-salon-type/{type}','PageController@getSalonType');
    Route::get('get-featured-salon','PageController@getFeaturedSalons');
    Route::post('suggestion','PageController@suggestion');
    Route::post('search','PageController@search');
    Route::get('get-salon/{name}','PageController@salon');
    Route::post('get-salon/{name}','PageController@salon');

    Route::post('respond-to-comment/{id}','salonController@respondToComment');
    Route::get('get-salon-comments/{id}','PageController@getSalonComment');


    Route::get('add-favorite-salon/{user}/{id}','userController@addFavorite');
    Route::get('remove-favorite-salon/{user}/{id}','userController@removeFavorite');

    Route::post('re-assign-appointments','salonController@reAssign');



    Route::post('book-service','userController@book');
    Route::post('get-bookings','userController@getMonthBooking');
    Route::post('accept-booking/{id}','userController@acceptBooking');
    Route::post('reject-booking/{id}','userController@rejectBooking');
    Route::get('finish-booking/{id}','userController@finishBooking');
    Route::get('delete-booking/{id}','userController@deleteBooking');
    Route::get('get-pending-booking','userController@getPendingBooking');
    Route::get('get-booking','userController@getBooking');

    Route::get('get-dashboard-with-date','salonController@getDashboard');



    Route::get('get-favorite-salons','userController@getFavoriteSalons');
    Route::get('remove-favorite-salon/{id}','userController@removeFavoriteSalon');
    Route::get('get-user-booking','userController@getUserBooking');
    Route::get('get-user-comments/{id}','userController@getUserComment');
    Route::get('delete-user-comments/{id}','userController@deleteUserComment');
    Route::post('update-user-comment/{id}','userController@updateUserComment');
    Route::get('check-user-has-comment/{salon}','userController@userHasComment');
    Route::post('comment-salon','userController@commentSalon');

    Route::post('working-hours','userController@workingHours');
    Route::post('schedule','userController@schedule');
    Route::get('get-schedule/{week}/{year}','userController@getSchedule');
    Route::post('add-shift','userController@addshift');
    Route::get('delete-shift/{id}','userController@deleteShift');


    Route::post('forgot-password', 'loginController@forget_password');
    Route::get('verify-reset-token', 'loginController@verify_password');
    Route::post('reset-password', 'loginController@reset_password');
});
