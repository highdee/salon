@extends('inc.admin_asset')
@section('vendorActive')
    active
@endsection
@section('content')
    <button type="button" class="btn btn-primary mr-1 mb-1 pl-3 pr-3" data-toggle="modal" data-target="#default"><i class="bx bx-plus"></i>Add Vendor</button>
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Vendor</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Vendor List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification_display')   
        <div class="row"  >
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h3 class="modal-title" id="myModalLabel1">Add New Owner</h3>
                            <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                                <i class="bx bx-x"></i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <section id="multiple-column-form">
                                <div class="row match-height">
                                    <div class="col-12">
                                        <div class="card-content">
                                           
                                            <div class="card-body p-0">
                                                <form class="form" method="post" action="adminAddVendor">
                                                    @csrf
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-6 col-12">
                                                                <label for="f-name-column">First Name</label>
                                                                <div class="form-label-group">
                                                                    <input type="text" id="f-name-column" class="form-control" placeholder="First Name" name="first_name">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <label for="l-name-column">Last Name</label>
                                                                <div class="form-label-group">
                                                                    <input type="text" id="l-name-column" class="form-control" placeholder="Last Name" name="last_name">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-12">
                                                                <label for="u-name-column">Username</label>
                                                                <div class="form-label-group">
                                                                    <input type="text" id="u-name-column" class="form-control" placeholder="Username" name="username">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <label for="first-name-column">Company's Name</label>
                                                                <div class="form-label-group">
                                                                    <input type="text" id="first-name-column" class="form-control" placeholder="Company's Name" name="company_name">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <label for="salon-name-column">Salon Name</label>
                                                                <div class="form-label-group">
                                                                    <input type="text" id="salon-name-column" class="form-control" placeholder="Salon Name" name="salon_name">
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-md-6 col-12">
                                                                <label for="email">Email</label>
                                                                <div class="form-label-group">
                                                                    <input type="text" id="email" class="form-control" name="email" placeholder="Email">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <label for="phone">Phone</label>
                                                                <div class="form-label-group">
                                                                    <input type="tel" id="phone" class="form-control" name="phone" placeholder="Phone Number">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <label for="phone">Subscription Date</label>
                                                                <fieldset class="form-group position-relative has-icon-left">
                                                                    <input type="text" class="form-control pickadate" placeholder="Select Date" name="sub_date">
                                                                    <div class="form-control-position">
                                                                        <i class='bx bx-calendar'></i>
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <label for="phone">Subscription Type</label>
                                                                <ul class="list-unstyled mb-0 mt-1">
                                                                    <li class="d-inline-block mr-2 mb-1">
                                                                        <fieldset>
                                                                            <div class="radio">
                                                                                <input type="radio" name="freq" id="radio1" value="month" checked>
                                                                                <label for="radio1">Monthly</label>
                                                                            </div>
                                                                        </fieldset>
                                                                    </li>
                                                                    <li class="d-inline-block mr-2 mb-1">
                                                                        <fieldset>
                                                                            <div class="radio">
                                                                                <input type="radio" name="freq" id="radio2" value="year">
                                                                                <label for="radio2">Yearly</label>
                                                                            </div>
                                                                        </fieldset>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    <div class="form-group col-6">
                                                                        {{-- <label>Generate Password</label> --}}
                                                                        <input type="text" class="form-control" name="password" id="password" placeholder="Click Button to generate Password" required>
                                                                    </div>
                                                                    <div class="form-group col-6">
                                                                        <button type="" id="generate" class="btn btn-primary">Generate Password</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{-- <div class="col-md-12 col-12">
                                                                <label for="address">Address</label>
                                                                <fieldset class="form-group">
                                                                    <textarea class="form-control" name="address" id="address" rows="3" placeholder="Address"></textarea>
                                                                </fieldset>
                                                            </div> --}}

                                                            <div class="col-12 d-flex justify-content-end">
                                                                <button type="submit" class="btn btn-primary btn-block mb-1 mt-0">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Username</th>
                                                <th>Company Name</th>
                                                <th>Salon</th>
                                                <th>Contact Phone</th>
                                                <th>Ban</th>
                                            </tr>
                                        </thead>
                                        <tbody class="table-hover">
                                            @foreach($vendors as $key => $vendor)
                                                <tr  onclick="window.location.href='/admin/vendor_show/{{$vendors[$key]->salon['id']}}'">
                                                    <td>{{$vendors[$key]->first_name}} {{$vendors[$key]->last_name}} </td>
                                                    <td>{{$vendors[$key]->owner['email']}}</td>
                                                    <td>{{$vendors[$key]->username}}</td>
                                                    <td>{{$vendors[$key]->company_name}}</td>
                                                    <td>{{$vendors[$key]->salon['name']}}</td>  
                                                    <td>{{$vendors[$key]->phone}}</td> 
                                                    <td>
                                                        <form action="/admin/banVendor" method="Post">
                                                            @csrf
                                                            <input type="hidden" value="{{$vendors[$key]->owner['id']}}" name="user_id">
                                                            <input type="submit" class="btn {{ $vendors[$key]->owner['status'] == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $vendors[$key]->owner['status'] == 0 ? 'active' : 'banned'}}" >
                                                            {{-- <input type="submit" class="btn {{ $vendor->status == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $vendor->status == 0 ? 'active' : 'banned'}}" > --}}
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        document.getElementById("generate").addEventListener('click',function(e){
            e.preventDefault();
            document.getElementById('password').value=Math.random().toString(36).slice(2); 
        });
    </script>
@endsection