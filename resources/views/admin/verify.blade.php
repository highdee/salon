@extends('inc.admin_asset')
@section('verifyActive')
	active
@endsection

@section('content')
	<div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Verify Vendors</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Verify
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('admin.message')
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    @if(count($vendors) < 1)
                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                            <div class="d-flex align-items-center">
                                <i class="bx bx-star"></i>
                                <span>
                                    No Unverified Vendor Available
                                </span>
                            </div>
                        </div>
                    @else
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body card-dashboard">
                                    <div class="table-responsive">
                                        <table class="table zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>Contact Person</th>
                                                    <th>Contact Phone</th>
                                                    <th>Whatsapp Contact</th>
                                                    <th>Salon Type</th>
                                                    <th>City</th>
                                                    <th>Contact Time</th>
                                            </thead>
                                            <tbody class="table-hover">
                                                @foreach($vendors as $vendor)
                                                    <tr onclick="window.location.href='/admin/verify_show/{{$vendor->user_id}}'">
                                                        <td>{{$vendor->contact_person}}</td>
                                                        <td>{{$vendor->contact_phone}}</td>
                                                        <td>{{$vendor->whatsapp_contact}}</td>    
                                                        <td>{{$vendor->salon_type}}</td>        
                                                        <td>{{$vendor->city}}</td>   
                                                        <td>{{$vendor->contact_time}}</td>                            
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Contact Person</th>
                                                    <th>Contact Phone</th>
                                                    <th>Whatsapp Contact</th>
                                                    <th>Salon Type</th>
                                                    <th>City</th>
                                                    <th>Contact Time</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection