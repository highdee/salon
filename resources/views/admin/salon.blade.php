@extends('inc.admin_asset')
@section('salonActive')
	active
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Salon</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">Salon List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification_display')   
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        {{-- <div class="card-header">
                            <h4 class="card-title">Zero configuration</h4>
                        </div> --}}
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                {{-- <p class="card-text">DataTables has most features enabled by default, so all you need to do to
                                    use it with your own tables is to call the construction function: $().DataTable();.</p> --}}
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Contact</th>
                                                <th>Address</th>
                                                <th>City</th>
                                                <th>Featured</th>
                                        </thead>
                                        <tbody class="table-hover">
                                            @foreach($salons as $salon)
                                                <tr>
                                                    <td onclick="window.location.href='/admin/salon_show/{{$salon->id}}'">{{$salon->name}}</td>
                                                    <td onclick="window.location.href='/admin/salon_show/{{$salon->id}}'">{{$salon->contact}}</td>
                                                    <td onclick="window.location.href='/admin/salon_show/{{$salon->id}}'">{{$salon->address}}</td>    
                                                    <td onclick="window.location.href='/admin/salon_show/{{$salon->id}}'">{{$salon->city}}</td>  
                                                    
                                                        <td>
                                                            <form action="admin/setFeatured" method="post">
                                                                @csrf
                                                                <input type="hidden" value="{{$salon->id}}" name="salon_id">
                                                                <input type="submit" class="btn {{ $salon->featured == 0 ? 'btn-danger' : 'btn-success'}}" value="{{ $salon->featured == 0 ? 'Not Featured' : 'Featured'}}">
                                                            </form> 
                                                        </td>
                                                    
                                                                      
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Contact</th>
                                                <th>Address</th>
                                                <th>City</th>
                                                <th>Featured</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection