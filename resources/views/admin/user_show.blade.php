@extends('inc.admin_asset')
@section('content')
<div class="content-header row">
</div>
<div class="content-body">
    @include('inc.notification_display') 
    <!-- page user profile start -->
    <section class="page-user-profile">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <!-- user profile nav tabs content start -->
                    <div class="col-lg-12">
                        <div class="tab-content">
                            <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-8">    
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <h5 class="card-title">User details</h5>
                                                    <ul class="list-unstyled">
                                                        <h4><li><i class="bx bx-user mr-50 mb-1"></i>{{$user->name}}</li></h4>
                                                        <h4><li><i class="cursor-pointer bx bx-envelope mb-1 mr-50"></i>{{$user->email}}</li></h4>
                                                        <h4><li><i class="cursor-pointer bx bx-map mb-1 mr-50"></i>{{$user->address}}</li></h4>
                                                        <h4><li><i class="cursor-pointer bx bx-phone-call mb-1 mr-50"></i>{{$user->contact}} </li></h4>
                                                        
                                                    </ul>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <h6><small class="text-muted">Whatsapp Contact</small></h6>
                                                            <p> <i class="cursor-pointer bx bxl-whatsapp-square mb-1 mr-50"></i>{{$user->whatsapp}}</p>
                                                        </div>
                                                        {{-- <div class="col-6">
                                                            <h6><small class="text-muted">Family Phone</small></h6>
                                                            <p>(+46) 454 22432</p>
                                                        </div> --}}
                                                        <div class="col-6">
                                                            <h6><small class="text-muted">Verified</small></h6>
                                                            <p>{{$user->email_verified_at}}</p>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <form action="/admin/banUser" method="Post">
                                                            @csrf
                                                            <input type="hidden" value="{{$user->id}}" name="user_id">
                                                            <input type="submit" class="btn {{ $user->status == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $user->status == 0 ? 'active' : 'banned'}}" >
                                                            
                                                            <a href="/admin/deleteUser/{{$user->id}}" class="btn btn-danger">Delete</a>
                                                        </form>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                    <div class="col-md-4">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body text-center">
                                                        
                                                        <div class="text-muted">Number of Bookings</div>
                                                        <h3 id="all">{{count($user->booking)}}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body text-center">
                                                        <div class="text-muted">No Show</div>
                                                        
                                                        <h3>{{count($show)}}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection