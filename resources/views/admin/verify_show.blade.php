@extends('inc.admin_asset')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('content')
	<div class="content-body" data-select2-id="23">
        <!-- users edit start -->
        <section class="users-edit" data-select2-id="22">
            @include('admin.message')
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <ul class="nav nav-tabs mb-2" role="tablist">
                            <li class="nav-item current">
                                <a class="nav-link d-flex align-items-center active" id="account-tab" data-toggle="tab" href="#account" aria-controls="account" role="tab" aria-selected="true">
                                    <i class="bx bx-user mr-25"></i><span class="d-none d-sm-block">Account</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" data-select2-id="21">
                            <div class="tab-pane active" id="account" aria-labelledby="account-tab" role="tabpanel">
                                <!-- users edit account form start -->
                                <form novalidate="" method="post" action="/admin/verify">
                                    @csrf
                                    <input type="hidden" name="userid" value="{{$users->id}}">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label>Username</label>
                                                    <input type="text" class="form-control" placeholder="Username" value="{{$users->username}}"  disabled>
                                                <div class="help-block"></div></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label>Contact Person</label>
                                                    <input type="text" class="form-control" placeholder="Name" value="{{$vendors->contact_person}}" disabled>
                                                <div class="help-block"></div></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label>E-mail</label>
                                                    <input type="email" class="form-control" placeholder="Email" value="{{$users->email}}" disabled>
                                                <div class="help-block"></div></div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label>Phone</label>
                                                    <input type="text" class="form-control" placeholder="Phone number" value="{{$vendors->contact_phone}}" disabled>
                                                <div class="help-block"></div></div>
                                            </div>
                                            <div class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control" placeholder="{{$vendors->city}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Contact Time</label>
                                                <input type="text" class="form-control" placeholder="{{$vendors->contact_time}}" disabled>
                                            </div>                                
                                        </div>
                                        <div class="col-6 mt-1">
                                            <div class="row">
                                                <div class="form-group col-6">
                                                    {{-- <label>Generate Password</label> --}}
                                                    <input type="text" class="form-control" name="password" id="password" placeholder="Click Button to generate Password" required>
                                                </div>
                                                <div class="form-group col-6">
                                                    <button type="" id="generate" class="btn btn-primary">Generate Password</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                            <button type="submit" class="btn btn-primary btn-block glow mb-1 mb-sm-0 mr-0 mr-sm-1">Verify Vendor</button>
                                        </div>
                                    </div>
                                </form>
                                <!-- users edit account form ends -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- users edit ends -->
    </div>
    <script>
        document.getElementById("generate").addEventListener('click',function(e){
            e.preventDefault();
            document.getElementById('password').value=Math.random().toString(36).slice(2); 
        });
    </script>
@endsection
