@extends('inc.admin_asset')
@section('hoverToOpen')
	menu-collapsed
@endsection
@section('app-asset')
    
@endsection
@section('content')
	<div class="content-header row">
    </div>
    <div class="content-body">
        <!-- page user profile start -->
        <section class="page-user-profile">
            <div class="row">
                <div class="col-12">
                    <!-- user profile heading section start -->
                    <div class="">
                        <div class="card-content">
                            <!-- user profile nav tabs start -->
                            <div class="card-body px-0">
                                <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                                    <li class="nav-item pb-0">
                                        <a class=" nav-link d-flex px-1 active" id="feed-tab" data-toggle="tab" href="#feed" aria-controls="feed" role="tab" aria-selected="true"><i class="bx bx-home"></i><span class="d-none d-md-block">Salon</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="features-tab" data-toggle="tab" href="#features" aria-controls="feature" role="tab" aria-selected="false"><i class="bx bx-copy-alt"></i><span class="d-none d-md-block">Features</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="service-tab" data-toggle="tab" href="#service" aria-controls="service" role="tab" aria-selected="false"><i class="bx bx-stats"></i><span class="d-none d-md-block">Services</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="gallery-tab" data-toggle="tab" href="#gallery" aria-controls="gallery" role="tab" aria-selected="false"><i class="bx bx-images"></i><span class="d-none d-md-block">Galleries</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="employees-tab" data-toggle="tab" href="#employees" aria-controls="employees" role="tab" aria-selected="false"><i class="bx bx-group"></i><span class="d-none d-md-block">Employees</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="comments-tab" data-toggle="tab" href="#comments" aria-controls="comments" role="tab" aria-selected="false"><i class="bx bx-message-alt"></i><span class="d-none d-md-block">Comments</span></a>
                                    </li>
                                    <li class="nav-item pb-0">
                                        <a class="nav-link d-flex px-1" id="bookings-tab" data-toggle="tab" href="#bookings" aria-controls="bookings" role="tab" aria-selected="false"><i class="bx bx-book-open"></i><span class="d-none d-md-block">Bookings</span></a>
                                    </li>
                                </ul>
                            </div>
                            <!-- user profile nav tabs ends -->
                        </div>
                    </div>
                    <!-- user profile heading section ends -->

                    <!-- user profile content section start -->
                    <div class="row">
                        <!-- user profile nav tabs content start -->
                        <div class="col-lg-12">
                           
                            <div class="tab-content">
                                <div class="tab-pane active" id="feed" aria-labelledby="feed-tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Basic details</h5>
                                                        <ul class="list-unstyled">
                                                            <li><i class="bx bx-user mr-50 mb-1"></i>{{$salon->name}}</li>
                                                            <li><i class="cursor-pointer bx bx-map mb-1 mr-50"></i>{{$salon->address}}</li>
                                                            <li><i class="cursor-pointer bx bx-map mb-1 mr-50"></i>{{$salon->city}}</li>
                                                            <li><i class="cursor-pointer bx bx-phone-call mb-1 mr-50"></i>{{$salon->contact}} </li>
                                                            {{-- <li><i class="cursor-pointer bx bx-envelope mb-1 mr-50"></i>Jonnybravo@gmail.com</li> --}}
                                                        </ul>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h6><small class="text-muted">Salon Type</small></h6>
                                                                <p> {{$salon->salon_type}}</p>
                                                            </div>
                                                            <div class="col-12">
                                                                <h6><small class="text-muted">Website</small></h6>
                                                                <p>{{$salon->websites}}</p>
                                                            </div>
                                                            <div class="col-12">
                                                                <h6><small class="text-muted">Description</small></h6>
                                                            <p>{{$salon->description}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-content">
                                                        <select class="mt-1 ml-1" id="selt" onchange="myFunction()" style="border:none; border-bottom:1px solid #bbb;">
                                                            <option id="sall" value="all">All</option>
                                                            <option id="fm" value="fmonth">1 month</option>
                                                            <option id="sm" value="smonth">6 month</option>
                                                            <option id="yr" value="yr">1 year</option>
                                                        </select>
                                                        <div class="card-body text-center">
                                                            
                                                            <div class="text-muted">Number of Bookings</div>
                                                            <h3 id="all">{{$allBooking}}</h3>
                                                            <h3 id="fmonth">{{$fmonthBooking}}</h3>
                                                            <h3 id="smonth">{{$smonthBooking}}</h3>
                                                            <h3 id="year">{{$yearBooking}}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-content">
                                                        <select class="mt-1 ml-1" id="total" onchange="totalFunc()" style="border:none; border-bottom:1px solid #bbb;">
                                                            <option value="all">All</option>
                                                            <option value="fmonth">1 month</option>
                                                            <option value="smonth">6 month</option>
                                                            <option value="yr">1 year</option>
                                                        </select>
                                                        <div class="card-body text-center">
                                                            <div class="text-muted">Amount Made</div>
                                                            
                                                            <h3 id="alm">{{$amount}}kn</h3>
                                                            <h3 id="ft">{{$ftotal}}kn</h3>
                                                            <h3 id="st">{{$stotal}}kn</h3>
                                                            <h3 id="yt">{{$ytotal}}kn</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-content">
                                                        <div class="card-body text-center">
                                                            <div class="text-muted">Employee</div>
                                                            <h3>{{count($salon->employees)}}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane container-fluid" id="features" aria-labelledby="features-tab" role="tabpanel"> 
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-content">
                                                    <div class="card-body">
                                                        <div class="col-12">
                                                            <table class="table table-borderless">
                                                                <tbody class="">
                                                                    <tr>
                                                                        <td>Payment Method:</td>
                                                                        <td>{{$salon->payment_methods}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Status:</td>
                                                                        <td>
                                                                            @if($salon->owner->status == 0)
                                                                                <span class="badge badge-light-success">Active</span> 
                                                                            @else
                                                                                <span class="badge badge-light-danger users-view-status">Banned</span>
                                                                            @endif                                                     
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <h5 class="mb-1"><i class="bx bx-link"></i> Social Links</h5>
                                                            <table class="table table-borderless">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Twitter:</td>
                                                                        <td><a href="#">{{$salon->twitter}}</a></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Facebook:</td>
                                                                        <td><a href="#">{{$salon->facebook}}</a></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Instagram:</td>
                                                                        <td><a href="#">{{$salon->instagram}}</a></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <h5 class="mb-1"><i class="bx bx-info-circle"></i> Other Info</h5>
                                                            <table class="table table-borderless mb-0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Features:</td>
                                                                        <td>{{$salon->features}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Home Services:</td>
                                                                        <td>{{$salon->home_services == 0 ? 'No' : 'Yes'}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Wifi:</td>
                                                                        <td>{{$salon->wifi == 0 ? 'No' : 'Yes'}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Parking:</td>
                                                                        <td>{{$salon->parking == 0 ? 'No' : 'Yes'}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Inviting Person:</td>
                                                                        <td>{{$salon->inviting_persons}}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </div>
                                <div class="tab-pane container-fluid" id="service" aria-labelledby="service-tab" role="tabpanel">
                                    @if(count($salon->services) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Service Available
                                                </span>
                                            </div>
                                        </div>
                                    @else    
                                        <div class="row">
                                        
                                            @foreach($salon->services as $service)
                                                <div class="col-md-4">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <table class="table table-borderless">
                                                                        <tbody class="">
                                                                            <tr>
                                                                                <td>Name:</td>
                                                                                <td class="users-view-username">{{$service->name}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Price:</td>
                                                                                <td class="users-view-email">{{$service->price}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Time:</td>
                                                                                <td>{{$service->time}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Features:</td>
                                                                                <td>{{$service->featured}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Action:</td>
                                                                                <td>{{$service->action}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Description:</td>
                                                                                <td>{{$service->description}}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            @endforeach         
                                        </div>
                                    @endif
                                </div>
                                <div class="tab-pane container-fluid" id="gallery" aria-labelledby="gallery-tab" role="tabpanel">
                                    @if(count($salon->images) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Gallery Available
                                                </span>
                                            </div>
                                        </div>
                                    @else 
                                        <div class="row">
                                            @foreach ($salon->images as $gal)
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="card">
                                                        <div class="card-content">
                                                        <img class="card-img-top img-fluid" src="{{$gal->filename}}" alt="Card image cap">
                                                            <div class="card-body">
                                                                <p class="card-text">
                                                                    {{$gal->description}}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                                <div class="tab-pane " id="employees" aria-labelledby="employees-tab" role="tabpanel">
                                    @if(count($salon->employees) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Employees Available
                                                </span>
                                            </div>
                                        </div>
                                    @else 
                                        <div class="content-body">
                                            <section id="basic-datatable">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-content">
                                                                <div class="card-body card-dashboard">
                                                                    <div class="table-responsive">
                                                                        <table class="table zero-configuration">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Name</th>
                                                                                    <th>Place</th>
                                                                                    <th>Office</th>
                                                                                    <th>contact</th>
                                                                                    <th>Services</th>
                                                                                    <th>Level</th>
                                                                                </tr>  
                                                                            </thead>
                                                                            <tbody class="table-hover"> 
                                                                                @foreach($salon->employees as $value)               
                                                                                    <tr>                                
                                                                                        <td>{{$value->name}}</td>
                                                                                        <td>{{$value->place}}</td>
                                                                                        <td>{{$value->office}}</td>
                                                                                        <td>{{$value->contact}}</td>
                                                                                        <td>{{$value->services}}</td>
                                                                                        <td>{{$value->level}}</td>
                                                                                    </tr>  
                                                                                @endforeach  
                                                                            </tbody>
                                                                            <tfoot>
                                                                                <tr>
                                                                                    <th>Name</th>
                                                                                    <th>Place</th>
                                                                                    <th>Office</th>
                                                                                    <th>contact</th>
                                                                                    <th>Services</th>
                                                                                    <th>Level</th>
                                                                                    
                                                                                </tr>
                                                                            </tfoot>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    @endif
                                </div>
                                <div class="tab-pane " id="comments" aria-labelledby="comments-tab" role="tabpanel">
                                    {{-- {{$salon->comments}} --}}
                                    @if(count($salon->comments) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Comments Available
                                                </span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    
                                                {{-- comments[0]->User->name --}}
                                                    <ul class="widget-timeline">
                                                        @foreach ($salon->comments as $com)
                                                            {{-- {{$com->User->name}} --}}
                                                            @if($com->rating > 4){
                                                                <li class="timeline-items timeline-icon-success active">
                                                                    <div class="timeline-time"> {{ $com->created_at }} </div>
                                                                    <h6 class="timeline-title">{{$com->User->name}}</h6> 
                                                                    <div class="timeline-content">
                                                                        {{$com->text}}
                                                                    </div>
                                                                </li>
                                                            }@elseif($com->rating <= 2)
                                                                <li class="timeline-items timeline-icon-primary active">
                                                                    <div class="timeline-time"> {{ $com->created_at }} </div>
                                                                    <h6 class="timeline-title">{{$com->User->name}}</h6>
                                                                    <div class="timeline-content">
                                                                        {{$com->text}}
                                                                    </div>
                                                                </li>
                                                            @else
                                                                <li class="timeline-items timeline-icon-danger active">
                                                                    <div class="timeline-time">{{ $com->created_at}}</div>
                                                                    <h6 class="timeline-title">{{$com->User->name}}</h6> 
                                                                    <div class="timeline-content">
                                                                        {{$com->text}}
                                                                    </div>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <!-- user profile nav tabs activity start -->
                                </div>
                                <div class="tab-pane container-fluid" id="bookings" aria-labelledby="bookings-tab" role="tabpanel">
                                    @if(count($salon->booking) < 1)
                                        <div class="alert alert-primary alert-dismissible mb-2" role="alert">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bx-star"></i>
                                                <span>
                                                    No Bookings Available
                                                </span>
                                            </div>
                                        </div>
                                    @else    
                                        <div class="row booking">
                                        
                                            @foreach($salon->booking as $bk)
                                                <div class="col-md-4">
                                                    <div class="card">
                                                        <div class="card-content">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <table class="table table-borderless">
                                                                        <tbody class="">
                                                                            <tr class="">
                                                                                <td>Customer Name:</td>
                                                                                <td class="users-view-username">{{$bk->customer->name}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Status:</td>
                                                                                <td>
                                                                                    @if($bk->status == 0)
                                                                                        <span class="badge badge-light-info">pending</span> 
                                                                                    @elseif($bk->status == 1)
                                                                                        <span class="badge badge-light-primary">Accepted</span>
                                                                                    @elseif($bk->status == 2)
                                                                                        <span class="badge badge-light-success">Completed</span>
                                                                                    @else
                                                                                        <span class="badge badge-light-danger">Rejected</span> 
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <?php $bk_ser = json_decode($bk->services) ?>
                                                                            @foreach($bk_ser as $hk=>$bk_val)
                                                                                <tr style="border-top:1px solid black;">
                                                                                    <td>Service:</td>
                                                                                    <td>{{$bk_ser[$hk]->name}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Time Taken:</td>
                                                                                    <td>{{$bk_ser[$hk]->time}}hours</td>
                                                                                </tr>
                                                                            @endforeach
                                                                            <tr>
                                                                                <td>Price:</td>
                                                                                <td class="users-view-email">{{$bk->price}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Date:</td>
                                                                                <td>{{$bk->date}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Time:</td>
                                                                                <td>{{$bk->time}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Employee:</td>
                                                                                <td>{{$bk->employee->name}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Review:</td>
                                                                                <td>{{$bk->reviewed}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Note:</td>
                                                                                <td>{{$bk->note}}</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            @endforeach         
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- user profile content section start -->
                </div>
            </div>
        </section>
        <!-- page user profile ends -->

    </div>

    <script>
        document.getElementById('ft').style.display = 'none';
        document.getElementById('st').style.display = 'none';
        document.getElementById('yt').style.display = 'none';
        document.getElementById('fmonth').style.display = 'none';
        document.getElementById('smonth').style.display = 'none';
        document.getElementById('year').style.display = 'none';
        function totalFunc(){
            var x= document.getElementById("total").value;
            if(x == 'smonth'){
                document.getElementById('st').style.display = 'block';
                document.getElementById('alm').style.display = 'none';
                document.getElementById('ft').style.display = 'none';
                document.getElementById('yt').style.display = 'none';
            }else if(x == 'fmonth'){
                document.getElementById('ft').style.display = 'block';
                document.getElementById('alm').style.display = 'none';
                document.getElementById('yt').style.display = 'none';
                document.getElementById('st').style.display = 'none';
            }else if(x == 'yr'){
                document.getElementById('yt').style.display = 'block';
                document.getElementById('alm').style.display = 'none';
                document.getElementById('st').style.display = 'none';
                document.getElementById('ft').style.display = 'none';
            }else{
                document.getElementById('alm').style.display = 'block';
                document.getElementById('ft').style.display = 'none';
                document.getElementById('st').style.display = 'none';
                document.getElementById('yt').style.display = 'none';
            }
           
        }
        function myFunction(){
            var x= document.getElementById("selt").value;
            if(x == 'smonth'){
                document.getElementById('smonth').style.display = 'block';
                document.getElementById('all').style.display = 'none';
                document.getElementById('fmonth').style.display = 'none';
                document.getElementById('year').style.display = 'none';
            }else if(x == 'fmonth'){
                document.getElementById('fmonth').style.display = 'block';
                document.getElementById('all').style.display = 'none';
                document.getElementById('year').style.display = 'none';
                document.getElementById('smonth').style.display = 'none';
            }else if(x == 'yr'){
                document.getElementById('year').style.display = 'block';
                document.getElementById('all').style.display = 'none';
                document.getElementById('smonth').style.display = 'none';
                document.getElementById('fmonth').style.display = 'none';
            }else{
                document.getElementById('all').style.display = 'block';
                document.getElementById('fmonth').style.display = 'none';
                document.getElementById('smonth').style.display = 'none';
                document.getElementById('year').style.display = 'none';
            }
           
        }
    </script>
@endsection

@section('script')
    
@endsection

