@extends('inc.admin_asset')
@section('userActive')
    active
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-12 mb-2 mt-1">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h5 class="content-header-title float-left pr-1 mb-0">Users</h5>
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb p-0 mb-0">
                            <li class="breadcrumb-item"><a href="/admin"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active">User List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        @include('inc.notification_display') 
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        {{-- <div class="card-header">
                            <h4 class="card-title">Zero configuration</h4>
                        </div> --}}
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                {{-- <p class="card-text">DataTables has most features enabled by default, so all you need to do to
                                    use it with your own tables is to call the construction function: $().DataTable();.</p> --}}
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Contact</th>
                                                <th>Email</th>
                                                <th>Address</th>
                                                <th>Ban</th>
                                                <th>Delete</th>
                                            </tr>  
                                        </thead>
                                        <tbody class="table-hover">
                                            @foreach($users as $user)
                                                {{-- onclick="window.location.href='/admin/user_show/{{$user->id}}'" --}}
                                                <tr>                          
                                                    <td><a href="/admin/user_show/{{$user->id}}"> {{$user->name}}</a></td>
                                                    <td><a href="/admin/user_show/{{$user->id}}">{{$user->contact}}</a></td>
                                                    <td><a href="/admin/user_show/{{$user->id}}">{{$user->email}}</a></td>
                                                    <td><a href="/admin/user_show/{{$user->id}}">{{$user->address}}</a></td> 
                                                    <td>
                                                        <form action="/admin/banUser" method="Post">
                                                            @csrf
                                                            <input type="hidden" value="{{$user->id}}" name="user_id">
                                                            <input type="submit" class="btn {{ $user->status == 0 ? 'btn-success' : 'btn-danger'}}" value="{{ $user->status == 0 ? 'active' : 'banned'}}" >
                                                        </form>
                                                    </td>
                                                    <td><a href="/admin/deleteUser/{{$user->id}}"><i class="bx bxs-trash"></i></a></td>         
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Contact</th>
                                                <th>Email</th>
                                                <th>Address</th>
                                                <th>WhatsApp</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection