@if (session()->has('message'))
    @if(session()->get('type') == 'success')
        <div class="alert alert-success text-center">
            <ul class="list-unstyled">
                <li>{{ session()->get('message') }}</li>
            </ul>
        </div>
    @else
        <div class="alert alert-danger  text-center">
            <ul class="list-unstyled">
                <li>{{ session()->get('message') }}</li>
            </ul>
        </div>
    @endif
@endif
@if ($errors->any())
    <div class="alert alert-danger  text-center">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
