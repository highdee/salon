<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Salon</title>
</head>
<body>


<div class="main-sectoin" style="font-family: 'Lato', sans-serif; max-width: 100%; width: 600px; margin: 0 auto;">
    <h3>Hi, {{$user}}</h3>
    <p>
        A customer just booked an appointment on the {{$date}}.
    </p>
    <p>Please login to your account to see more details</p>
    <h3><a href="{{$link}}">Goto my dashboard</a></h3>
</div>

</body>
</html>
