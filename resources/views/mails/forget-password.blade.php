<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Salon</title>
</head>
<body>


<div class="main-sectoin" style="font-family: 'Lato', sans-serif; max-width: 100%; width: 600px; margin: 0 auto;">
    <h3>Hi, {{$user}}</h3>
    <p>Click on below link to reset your password</p>
    <a href="{{$link}}">{{$link}}</a>
</div>

</body>
</html>
