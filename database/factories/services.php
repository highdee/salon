<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\service;
use Faker\Generator as Faker;

$factory->define(service::class, function (Faker $faker) {
    return [
    	'user_id'=>$faker->numberBetween(0,10),
        'salon_id'=>$faker->numberBetween(0,10),
        'name'=>$faker->name,
        'description'=>$faker->text,
        'price'=>$faker->numberBetween(10,200),
        'time'=>strftime("%B %d %Y, %X",time()),
        'featured'=>$faker->numberBetween(0,10),
        'action'=>$faker->numberBetween(0,10),
    ];
});
