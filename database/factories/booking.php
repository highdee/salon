<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\booking;
use Faker\Generator as Faker;

$factory->define(booking::class, function (Faker $faker) {
    return [
        'user_id'=>$faker->numberBetween(0,10),
        'salon_id'=>$faker->numberBetween(0,10),
        'employee_id'=>$faker->numberBetween(0,10),
        'user_name'=>$faker->username,
        'date'=>$faker->date,
        'time'=>strftime("%B %d %Y, %X",time()),
        'price'=>$faker->numberBetween(100,5000),
        'note'=>$faker->text,
        'services'=>$faker->text,
        'status'=>$faker->numberBetween(0,2)
    ];
});
