<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\vendor;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(vendor::class, function (Faker $faker) {
	$date = now(); // helper for Carbon::now()

	foreach (range(1, 480) as $i) { // 8*60 minutes

	    //do something with $date

	    $date->addMinute(); // increments by a minute
	}
    return [
        'first_name'=>$faker->firstName,
        'last_name'=>$faker->lastName,
        'company_name'=>$faker->company,
        'username'=>$faker->userName,
        'user_id'=>$faker->numberBetween(0,10),
        'salon_id'=>$faker->numberBetween(0,10),
    	// 'city'=>$faker->city,
        'phone'=>$faker->phoneNumber,
        'sub_date'=>$faker->date,
        'freq'=>$faker->numberBetween(0,1) == 0?'month':'year',
        'due_date'=>$faker->date,
        'verified'=>$faker->date,
        // 'salon_type'=>$faker->word,
        'created_at'=>now(),
        // 'email_verified_at' => now(),

       // 'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        
    ];
});
