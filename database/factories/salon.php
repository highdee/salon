<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\salon;
use Faker\Generator as Faker;

$factory->define(salon::class, function (Faker $faker) {
    return [
        'user_id'=>$faker->numberBetween(0,10),
        'name'=>$faker->name,
        'description'=>$faker->text($maxNbChars = 200),
        'city'=>$faker->city,
        'employee'=>$faker->name,
        'contact'=>$faker->phoneNumber,
        'address'=>$faker->address,
        // 'working_hours'=>'8:00' . ' - '.strftime("%X"),
        'employee'=>$faker->name,
        'features'=>$faker->text($maxNbChars = 50),
        // 'socials'=>$faker->email,
    ];
});
