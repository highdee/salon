<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\employee;
use Faker\Generator as Faker;

$factory->define(employee::class, function (Faker $faker) {
    return [
       'user_id'=>$faker->numberBetween(0,10),
       'salon_id'=>$faker->numberBetween(0,10),
       'name'=>$faker->name,
       'level'=>$faker->numberBetween(0,3),
       'place'=>$faker->address,
    //    'office'=>$faker->address,
       'contact'=>$faker->phoneNumber,
       'services'=>$faker->text
    ];
});
