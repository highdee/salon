<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->string('city')->nullable();
            $table->string('contact')->nullable();
            $table->string('address')->nullable();
            $table->string('employee')->nullable();
            $table->longText('features')->nullable();
            $table->string('payment_methods')->nullable();
            $table->longText('map')->nullable();
            $table->longText('salon_type')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('website')->nullable();
            $table->integer('home_service')->default(0);
            $table->integer('wifi')->default(0);
            $table->integer('parking')->default(0);
            $table->integer('status')->default(0);
            $table->integer('featured')->nullable();
            $table->integer('inviting_persons')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salons');
    }
}
