<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('salon_id');
            $table->integer('employee_id');
            $table->string('user_name')->nullable();
            $table->date('date');
            $table->string('time');
            $table->string('price');
            $table->longText('note')->nullable();
            $table->longText('vendor_note')->nullable();
            $table->longText('services');
            $table->integer('status');
            $table->integer('notified')->default(0);
            $table->date('reviewed')->nullable();
            $table->softDeletes()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
