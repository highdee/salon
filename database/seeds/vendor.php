<?php

use Illuminate\Database\Seeder;

class vendor extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\vendor', 10)->create();
    }
}
