<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(adminSeeder::class);
        // $this->call(UserSeeder::class);
        // $this->call(vendor::class);
        // $this->call(serviceSeeder::class);
        // $this->call(salonSeeder::class);
        // $this->call(employeeSeeder::class);
        // $this->call(bookingSeeder::class);
    }
}
