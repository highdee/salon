<?php

use Illuminate\Database\Seeder;

class salonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\salon', 10)->create();
    }
}
